import "./styles/index.scss";
import App from "./scripts/App";

window.addEventListener("DOMContentLoaded", () => {
    const app = new App();

    // init app, pass the template 
    app.init(/8080|8888/.test(window.location.port) ? "http://localhost:8888/Sourcetree/TT/phaser-pwa/dist/" : "./template/");

    // assign it globally to the Window
    (window as any).app = app;
}, false);