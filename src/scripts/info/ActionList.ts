import { ActionType, TimelineEvent } from "lecture-timeline";
import MyTimeline from "../timeline/MyTimeline";
import MyAction from "../timeline/MyAction";
import { debounce, secondsToString, swapNodes } from "../utils";
import MyTrack from "../timeline/MyTrack";
import MyClip from "../timeline/MyClip";
import TimelinePane from "../timeline/TimelinePane";
import InfoPane from "./InfoPane";
import Modal from "../Modal";
import StagePane, { StagePaneEvent } from "../stage/StagePane";

export default class ActionList extends EventTarget {
    element: HTMLElement;
    currentDrag: HTMLElement | null;

    constructor(element: HTMLElement) {
        super();

        this.element = element;
    }

    setupEvents() {
        const ftnInputUpdate = debounce(() => this.onInputUpdate());
        InfoPane.instance.element.addEventListener("input", () => ftnInputUpdate());

        const ftnUpdateTime = debounce(() => this.onUpdateTime());
        MyTimeline.instance.addEventListener("selectedTrack", () => window.requestAnimationFrame(() => this.onUpdateTime() ));
        MyTimeline.instance.addEventListener(TimelineEvent.ActionAdded, () => ftnUpdateTime());
        MyTimeline.instance.addEventListener(TimelineEvent.ActionRemoved, () => ftnUpdateTime());
        MyTimeline.instance.loader.addEventListener(TimelineEvent.LoaderResourceAdd, () => ftnUpdateTime());
        TimelinePane.instance.drawer.addEventListener("updateTime", () => ftnUpdateTime());

        StagePane.instance.addEventListener(StagePaneEvent.UpdatedDisplay, (event: Event) => this.onUpdateDisplay(event as CustomEvent));

        window.addEventListener("message", (event: MessageEvent) => {
            switch (event.data.type) {
                // user double clicked on an element in the iFrame
                case "selectedAction": {
                    this.onSelectedAction(event.data.actionId);
                    break;
                }
            }
        });
    }

    onInputUpdate() {
        this.checkDependencyErrors();
    }

    onUpdateTime() {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        // show the actions
        let myActions: Array<MyAction> = [];
        if (this.selectedTrack) {
            myActions = this.selectedTrack.getActionsByTime(this.time);
        }

        // show them
        this.reset(myActions);
    }

    onUpdateDisplay(event: CustomEvent) {
        // if no active element, admin is probably dragging along the timeline
        const activeDiv = document.activeElement?.closest(".action[data-action-id]");
        if (!activeDiv) {
            return;
        }

        // update the Action's editor
        Array.from(this.element.querySelectorAll(".action[data-action-id]")).forEach((div: any) => {
            if (activeDiv !== div) {
                const actionId = parseInt(div.dataset.actionId);
                const action = MyTimeline.instance.getAction(actionId) as any;
                if (action) {
                    action.editorRef.editorHTML(div.querySelector("div.row.action-options"));
                }
            }
        });
    }

    onSelectedAction(actionId: number) {
        // find the Action by id
        const action = MyTimeline.instance.getAction(actionId) as any;
        if (action) {
            const myTrack = MyTimeline.instance.getTrack(action.track.id);
            if (myTrack) {
                let switched = false;

                // go to the track
                if (this.selectedTrack !== myTrack) {
                    switched = true;
                    MyTimeline.instance.selectedTrack = myTrack as MyTrack;
                }

                // go to that point in time
                TimelinePane.instance.time = action.startTime;

                // mimic double click on the Action to expand
                setTimeout(() => this.showAction(actionId)?.scrollIntoView(), 500); // wait for the debounce
            }
        }
    }

    reset(actions: unknown[]) {
        // get the current Actions
        const opened: Array<string> = [];
        this.element.querySelectorAll(`.action[data-action-id][draggable="false"]`).forEach(li => {
            opened.push((li as HTMLElement).dataset.actionId!);
        });

        // empty
        this.element.innerHTML = "";

        // add the actions to the list
        actions.forEach(action => {
            this.add(action as MyAction);
        });

        // open the previously opened
        opened.forEach(open => {
            this.showAction(parseInt(open))?.scrollIntoView();
        });
    }
    
    add(myAction: MyAction) {
        const li: HTMLElement = document.createElement("div");
        li.classList.add("action");
        li.dataset.actionId = myAction.id;
        li.dataset.draggable = "true";
        li.draggable = true;

        // is it out of order maybe?
        if (!myAction.isValid) {
            li.classList.add("error");
        }

        // make the context
        li.innerHTML = `
            <div class="action__header">
                <span class="title">${myAction.origAction.editorName()}</span>
                <span class="material-icons hidden"></span>
                <span class="material-icons locked"></span>
                <span class="material-icons enabled"></span>
            </div>

            <div class="action__content row">
                <!--
                <div class="row">
                    <div class="three columns">
                        Type
                    </div>
                    <div class="nine columns">
                        <select name="action-type" class="u-full-width"></select>
                    </div>
                </div>
                -->
                <div class="row">
                    <div class="three columns">
                        Name
                    </div>
                    <div class="nine columns">
                        <input type="text" name="name" class="u-full-width" value="${myAction.name}" autocomplete="off" />
                    </div>
                </div>
                <div class="row action-options">
                    <!-- will be populated -->
                </div>
                <div class="row">
                    <div class="nine columns offset-by-three">
                        <button class="button button-red remove-action">
                            <span class="material-icons">delete</span>
                        </button>
                    </div>
                </div>
            </div>
        `;

        // add to element
        this.element.appendChild(li);

        // let the Action create it's html and events
        this.updateActionOptions(myAction);

        // apply the events to this list item
        this.applyEvents(myAction, li);
    }

    applyEvents(myAction: MyAction, li: HTMLElement) {
        const spanDrag = li.querySelector(".action__header .title");
        const spanDelete = li.querySelector(".action__content .remove-action");
        const spanTitle = li.querySelector(".action__header .title");
        let delay = 0; // for the dragging

        // init user drag
        li.addEventListener("mousedown", (event) => this.currentDrag = event.target as HTMLElement);

        // did the admin ddrag the span element
        li.addEventListener("dragstart", (event) => {
            if (this.currentDrag === spanDrag) {
                // event.dataTransfer!.setData("text/plain", "handle"); // for FireFox
                // hide the child content so we don't get weird ghosting
                (li.querySelector(".action__content.row") as HTMLElement)!.style.display = "none";
            }
        });

        // stop drag
        li.addEventListener("dragend", (event) => {
            event.preventDefault();

            // show it again
            (li.querySelector(".action__content.row") as HTMLElement)!.style.display = "";

            // done with it
            this.currentDrag = null;
        });

        li.addEventListener("dragover", (event) => {
            event.preventDefault();
            event.dataTransfer!.dropEffect = "move";

            // so the user doesn't drag an file from the desktop over this
            if (event.dataTransfer?.files.length) {
                return;
            }
            if (!this.currentDrag) {
                return;
            }

            const currentItem = this.currentDrag!.closest("[data-action-id]") as HTMLElement;
            const overItem = (event.target as HTMLElement).closest("[data-action-id]") as HTMLElement;

            // currently in motion, don't proceed
            if (currentItem.getAnimations().length || overItem.getAnimations().length) {
                return;
            }
            // this is a catch, sometimes the `animate` gets executed twice
            if (Date.now() < delay + 240) {
                return;
            }

            // check our current drag item with the item we are over
            if (currentItem !== overItem) {
                // get bounds
                const currentRect = currentItem?.getBoundingClientRect()!;
                const overRect = overItem?.getBoundingClientRect()!;

                // need to be more centered with the item
                if (event.clientY < overRect.y + overRect.height * 0.2 || event.clientY > overRect.y + overRect.height * 0.8) {
                    return;
                }

                // move the new (over) element to the current
                let top = currentRect.top - overRect.top;
                let left = currentRect.left - overRect.left;
                overItem.animate([{ transform: `translate(${left}px, ${top}px)` }], { duration: 120 });

                // move the current to the new (over) element
                top = overRect.top - currentRect.top;
                left = overRect.left - currentRect.left;
                currentItem.animate([{ transform: `translate(${left}px, ${top}px)` }], { duration: 120 }).addEventListener("finish", () => {
                    swapNodes(currentItem, overItem);
                });

                delay = Date.now();
            }
        });

        // done dragging
        li.addEventListener("drop", () => {
            this.onDrop();
        });

        // open the contents of it
        spanTitle!.addEventListener("click", (event) => {
            // open in the modal
            if ((event as KeyboardEvent).shiftKey) {
                Modal.instance.show("Edit Action");
                myAction.editorHTML(Modal.instance.elements.content);
            }
            // slide open
            else {
                // show the content
                const content = li.querySelector(".action__content") as HTMLElement;

                // seems odd, but this is needed because the transition won't work with the value of "auto"
                content.style.height = content.offsetHeight > 0 ? content.offsetHeight + "px" : "0px";
                content.style.height = content.offsetHeight > 0 ? "0px" : content.scrollHeight + "px";
                li.draggable = content.offsetHeight > 0; // make it draggable/sortable

                content.addEventListener("transitionend", function transitionEnd() {
                    content.removeEventListener("transitionend", transitionEnd);

                    if (content.offsetHeight > 0) {
                        content.style.height = "auto";
                    }
                });
            }
        });

        // delete it
        spanDelete!.addEventListener("dblclick", (event) => {
            this.selectedTrack.removeAction(myAction);
        });

        // the Action type actions are applied here

        // name input
        const inputName = <HTMLInputElement> li.querySelector(`input[name="name"]`);
        inputName.addEventListener("input", () => {
            myAction.name = inputName.value;
        });

        // enabled
        const spanEnabled = <HTMLSpanElement> li.querySelector(`span.material-icons.enabled`);
        spanEnabled.addEventListener("click", () => {
            myAction.origAction.enabled = !myAction.origAction.enabled;
            updateCheckboxSpans();
            this.element.dispatchEvent(new CustomEvent("input", { bubbles: true }));
        });

        // locked
        const spanLocked = <HTMLSpanElement> li.querySelector(`span.material-icons.locked`);
        spanLocked.addEventListener("click", () => {
            myAction.origAction.locked = !myAction.origAction.locked;
            updateCheckboxSpans();
            this.element.dispatchEvent(new CustomEvent("input", { bubbles: true }));
        });

        // locked
        const spanHidden = <HTMLSpanElement> li.querySelector(`span.material-icons.hidden`);
        spanHidden.addEventListener("click", () => {
            myAction.origAction.hidden = !myAction.origAction.hidden;
            updateCheckboxSpans();
            this.element.dispatchEvent(new CustomEvent("input", { bubbles: true }));
        });

        // update the spans - show the enabled/disable, locked/unlocked, and visibility
        function updateCheckboxSpans() {
            const action = myAction.origAction;

            spanEnabled.innerText = action.enabled ? "check_box" : "check_box_outline_blank";
            spanLocked.innerText = action._locked ? action.locked ? "lock" : "lock_open" : "";
            spanHidden.innerText = action._hidden ? action.hidden ? "visibility_off" : "visibility" : "";

            // can't apply "hidden"???

            spanLocked.classList.remove("hidden");
            spanHidden.classList.remove("hidden");
            
            if (!spanLocked.innerText) {
                spanLocked.classList.add("hidden");
            }
            if (!spanHidden.innerText) {
                spanHidden.classList.add("hidden");
            }
        };
        updateCheckboxSpans();

        // // add our event types
        // const selectType = <HTMLSelectElement> li.querySelector(`select[name="action-type"]`);
        // Object.keys(actionTypes).forEach(actionType => {
        //     const option = document.createElement("optgroup");
        //     option.label = actionType;
        //     if (actionType !== "") {
        //         option.setAttribute("disabled", "disabled");
        //         selectType.options.add(option);
        //     }
        //     actionTypes[actionType].forEach((action: any) => {
        //         const option = document.createElement("option");
        //         option.innerHTML = (actionType ? " &nbsp; " : "") + action.label;
        //         option.value = action.type;
        //         option.selected = action.type === myAction.origAction.type;
        //         selectType.options.add(option);
        //     });
        // });
        // selectType.addEventListener("input", () => {
        //     // get new type
        //     const newType = selectType.options[selectType.selectedIndex].value;

        //     // find existing action index 
        //     const actionIndex = this.selectedTrack.actions.indexOf(myAction.origAction) || 0;

        //     // remove the old
        //     this.selectedTrack.removeAction(myAction);

        //     // create the new Action
        //     const type = newType as keyof typeof ActionType;
        //     myAction = new MyAction(new ActionType[type]({ id: myAction.id, clipId: myAction.clipId, startTime: myAction.startTime }));
        //     this.selectedTrack.addAction(myAction);

        //     // set the new Action in existing place - we MUST do `array.splice` twice - the `addAction` will call `sortActions` and mess with `actions` array
        //     this.selectedTrack.actions.splice(this.selectedTrack.actions.indexOf(myAction.origAction), 1);
        //     this.selectedTrack.actions.splice(actionIndex, 0, myAction.origAction);

        //     // adjust visually (again, because we called `array.splice`)
        //     this.selectedTrack.adjustActions();

        //     // update the Action options html
        //     this.updateActionOptions(myAction);

        //     // updates the visibility, locked, checkboxes
        //     updateCheckboxSpans();

        //     // check if any relation errors
        //     this.checkDependencyErrors();
        // });
    }

    // updates the event options
    updateActionOptions(myAction: MyAction) {
        const li = this.element.querySelector(`div[data-action-id="${myAction.id}"] div.row.action-options`);
        if (li) {
            myAction.editorHTML(li as HTMLElement);
        }
    }

    updateTitles(myActions: MyAction[]) {
        myActions.forEach(myAction => {
            const spanTitle = this.element.querySelector(`div[data-action-id="${myAction.id}"] span.title`);
            if (spanTitle) {
                spanTitle!.innerHTML = myAction.origAction.editorName();
            }
        });
    }

    showAction(actionId: number) {
        const element = this.element.querySelector(`div[data-action-id="${actionId}"]`) as HTMLElement;
        if (element && element.draggable) {
            const span = element.querySelector("span.title") as HTMLSpanElement;
            span.dispatchEvent(new CustomEvent("click"));
            return span;
        }
    }

    onDrop() {
        // get the id's from the Event
        const actionIds: Array<number> = [];
        this.element.querySelectorAll("div[data-draggable][data-action-id]").forEach(li => {
            const actionId = parseInt((li as HTMLElement).dataset.actionId!);
            actionIds.push(actionId);
        });

        // call out our new array by Action id's
        this.selectedTrack.sortActionsById(actionIds);

        this.checkDependencyErrors();

        this.dispatchEvent(new CustomEvent("actionsSorted"));
    }

    checkDependencyErrors() {
        Array.from(this.element.querySelectorAll(`div[data-draggable][data-action-id]`)).forEach(li => {
            const element = li as HTMLElement;
            const actionId = parseInt(element.dataset.actionId!);
            const action = this.selectedTrack.getAction(actionId) as any;

            element.classList.remove("error");
            if (action && !action.isValid) {
                element.classList.add("error");
            }
        });
    }

    get selectedTrack() {
        return <MyTrack> MyTimeline.instance.selectedTrack;
    }
    get selectedClip() {
        return <MyClip> this.selectedTrack?.selectedClip;
    }
    get time() {
        return TimelinePane.instance.time;
    }
}