import StagePane from "../stage/StagePane";
import { clamp, debounce } from "../utils";

export default class Properties extends EventTarget {
    static instance: Properties;

    element: HTMLElement;
    elements: {[element: string]: HTMLElement} = {};
    inputs: {[element: string]: HTMLInputElement} = {};

    constructor(element: HTMLElement) {
        super();

        this.element = element;

        this.inputs.x = this.element.querySelector(".x input") as HTMLInputElement;
        this.inputs.y = this.element.querySelector(".y input") as HTMLInputElement;
        this.inputs.width = this.element.querySelector(".width input") as HTMLInputElement;
        this.inputs.height = this.element.querySelector(".height input") as HTMLInputElement;
    }

    setupEvents() {

        const ftnOnInput = debounce((event: Event) => this.onInput(event), 400);
        this.element.addEventListener("input", (event) => ftnOnInput(event));

        window.addEventListener("message", (event) => {
            const data = event.data;

            switch (data.type) {
                case "selected-bounds": {

                    const rect = data.rect;
                    this.inputs.x.value = rect.x < Infinity ? rect.x.toFixed(2) : "0";
                    this.inputs.y.value = rect.y < Infinity ? rect.y.toFixed(2) : "0";
                    this.inputs.width.value = rect.width > -Infinity ? rect.width.toFixed(2) : "0";
                    this.inputs.height.value = rect.height > -Infinity ? rect.height.toFixed(2) : "0";

                    break;
                }
            }
        });
    }

    onInput(event: Event) {
        const x = clamp(parseFloat(this.inputs.x.value), 0, 100);
        const y = clamp(parseFloat(this.inputs.y.value), 0, 100);
        const w = clamp(parseFloat(this.inputs.width.value), 0, 100);
        const h = clamp(parseFloat(this.inputs.height.value), 0, 100);

        StagePane.instance.post({
            type: "updateRectAndMatrix",
            rect: new DOMRect(x, y, w, h),
            matrix: new DOMMatrix()
        })
    }
}