import { TimelineEvent } from "lecture-timeline";
import MyClip from "../timeline/MyClip";
import MyTimeline from "../timeline/MyTimeline";
import MyTrack from "../timeline/MyTrack";
import TimelinePane from "../timeline/TimelinePane";
import { debounce, secondsToString } from "../utils";

export default class ClipPreview extends EventTarget {
    static instance: ClipPreview;
    
    element: HTMLElement;
    elements: {[element: string]: HTMLElement} = {};
    
    video: HTMLVideoElement;
    isPlaying = false;
    
    constructor(element: HTMLElement) {
        super();

        this.element = element;

        this.video = document.createElement("video") as HTMLVideoElement;

        this.elements.buttonRemove = this.element.querySelector("button.remove-clip") as HTMLElement;
        this.elements.buttonAdd = this.element.querySelector("button.add-clip") as HTMLElement;
        this.elements.buttonPreview = this.element.querySelector("button.preview") as HTMLElement;
    }

    setupEvents() {
        this.element.addEventListener("click", (event: Event) => this.onClick(event as CustomEvent));
        this.element.addEventListener("dblclick", (event: Event) => this.onDoubleClick(event as CustomEvent));

        // video is done
        this.video.addEventListener("ended", () => {
            this.isPlaying = false;
            this.elements.buttonPreview.querySelector("span")!.innerText = "play_arrow";
        });

        // listen for new resources being added
        MyTimeline.instance.loader.addEventListener(TimelineEvent.LoaderResourceAdd, (event) => this.onLoaderResource(event));

        const ftnUpdateTime = debounce(() => this.onUpdateTime());
        TimelinePane.instance.drawer.addEventListener("updateTime", () => ftnUpdateTime());
        MyTimeline.instance.addEventListener("selectedTrack", () => ftnUpdateTime());
    }

    onLoaderResource(event: Event) {
        const data = (event as CustomEvent).detail;
        const resource = data.resource;

        // if it's a navigator voice
        if (resource.type === "sound" && resource.kind === "nv") {
            const selectClip = <HTMLSelectElement> this.element.querySelector(`select`);
            const option = document.createElement("option");
            option.text = resource.title;
            option.value = resource.name;
            selectClip.options.add(option);
        }
    }

    async onClick(event: Event) {
        const element = event.target as HTMLElement;

        // get the source from the select
        const selectClip = <HTMLSelectElement> this.element.querySelector(`select`);
        const resourceName = selectClip.options[selectClip.selectedIndex].value;

        // add a clip
        if (element === this.elements.buttonAdd) {
            // empty
            if (resourceName === "") {
                return;
            }

            // load the source
            await MyTimeline.instance.loader.loadResource(resourceName);

            const myClip = new MyClip({ resourceName: resourceName });
            this.prepNewClip(myClip);

            // update
            this.onUpdateTime();
        }
        // preview
        else if (element === this.elements.buttonPreview) {

            // if playing, pause the video
            if (this.isPlaying) {
                this.isPlaying = false;
                element.querySelector("span")!.innerText = "play_arrow";

                this.video.pause();
            }
            // start playing the audio
            // will check if we need to load it
            else {
                // empty
                if (resourceName === "") {
                    return;
                }

                this.isPlaying = true;
                element.querySelector("span")!.innerText = "pause";

                // load the source
                await MyTimeline.instance.loader.loadResource(resourceName);

                // remove the old sources from the video
                Array.from(this.video.querySelectorAll("source")).forEach(source => {
                    if (source.dataset.resourceName !== resourceName) {
                        source.remove();
                    }
                });

                // add the new source
                if (!this.video.querySelectorAll("source").length) {
                    const source = document.createElement("source");
                    source.dataset.resourceName = resourceName;
                    source.type = "video/mp4";
                    source.src = MyTimeline.instance.loader.getResource(resourceName).dataURL;
                    this.video.appendChild(source);

                    // load it
                    this.video.load();
                }

                // start
                this.video.play();
            }
        }
    }

    onDoubleClick(event: CustomEvent) {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        const element = event.target as HTMLElement;
        const track = this.selectedTrack;

        // remove the clip
        if (element.classList.contains("remove-clip")) {
            if (track && track.selectedClip) {                
                track.removeClip(track.selectedClip);
                track.checkClips();
                this.onUpdateTime();
            }
        }
    }

    onUpdateTime() {
        // replace Clip, and show ability to Remove Clip button
        this.elements.buttonAdd.querySelector("span")!.innerHTML = this.selectedClip ? "download" : "add";
        this.elements.buttonRemove.style.display = !this.selectedClip ? "none" : "block";

        // show the selected clip in the drop down
        const selectClip = <HTMLSelectElement> this.element.querySelector("select");
        if (selectClip.options.length && this.selectedClip) {
            selectClip.value = this.selectedClip.resourceName;
        }

        // update some info
        this.element.querySelector(".nine.columns.start-time")!.innerHTML = secondsToString(this.selectedClip ? this.selectedClip.startTime : 0);
        this.element.querySelector(".nine.columns.end-time")!.innerHTML = secondsToString(this.selectedClip ? this.selectedClip.endTime : 0);
        this.element.querySelector(".nine.columns.duration")!.innerHTML = secondsToString(this.selectedClip ? this.selectedClip.audioLength : 0);
    }

    prepNewClip(myClip: MyClip) {
        const isMulti = false;        

        // create the new Clip
        const selectedTrack = this.selectedTrack;
        const selectedClip = this.selectedClip;

        // replace existing
        if (selectedClip && !isMulti) {
            const clipIndex = selectedTrack.clips.indexOf(selectedClip);

            // set the start time
            myClip.startTime = selectedClip.startTime;

            // find out the spacing between each Clip
            const spacing = [];
            for (let i = clipIndex; i < selectedTrack.clips.length; i++) {
                if (selectedTrack.clips[i + 1]) {
                    spacing.push(selectedTrack.clips[i + 1].startTime - selectedTrack.clips[i].endTime);
                }
            }

            // replace Clip in array
            selectedTrack.removeClip(selectedClip);
            selectedTrack.addClip(myClip);
            selectedTrack.clips.splice(clipIndex, 1, myClip);

            // adjust the starting time for Clips in front
            for (let i = clipIndex + 1; i < selectedTrack.clips.length; i++) {
                selectedTrack.clips[i].startTime = selectedTrack.clips[i - 1].endTime + (spacing.shift() || 0);
            }
        }
        else {
            // set the start time
            myClip.startTime = isMulti && selectedClip ? (selectedClip.endTime + 1).roundToTenth() : this.time;

            // check if we can do a clean insert
            let cleanInject = true;
            selectedTrack.clips.forEach(clip => {
                if (myClip.startTime + myClip.audioLength >= clip.startTime && myClip.startTime <= clip.endTime) {
                    cleanInject = false;
                }
            });

            // nice and tidy
            if (cleanInject) {
                selectedTrack.addClip(myClip);
            }
            // oh drat
            else {
                let clipIndex = selectedTrack.clips.findIndex(clip => clip.startTime >= myClip.startTime);
                if (clipIndex < 0) {
                    // append to the end of the clip we are over
                    selectedTrack.addClip(myClip);
                    clipIndex = selectedTrack.clips.length - 1;
                }
                const spacing = [selectedTrack.clips[clipIndex].startTime - myClip.startTime];

                // find out the spacing between each Clip
                for (let i = clipIndex; i < selectedTrack.clips.length; i++) {
                    if (selectedTrack.clips[i + 1]) {
                        spacing.push(selectedTrack.clips[i + 1].startTime - selectedTrack.clips[i].endTime);
                    }
                }

                // add Clip to Track
                if (selectedTrack.clips.indexOf(myClip) < 0) {
                    selectedTrack.addClip(myClip);
                }

                // adjust the starting time for Clips in front
                for (let i = clipIndex + 1; i < selectedTrack.clips.length; i++) {
                    selectedTrack.clips[i].startTime = selectedTrack.clips[i - 1].endTime + (spacing.shift() || 1).roundToTenth();
                }
            }
        }

        selectedTrack.render();
        selectedTrack.checkClips();
        selectedTrack.selectedClip = myClip;
    }


    get selectedTrack() {
        return <MyTrack> MyTimeline.instance.selectedTrack;
    }
    get selectedClip() {
        return <MyClip> this.selectedTrack?.selectedClip;
    }
    get time() {
        return TimelinePane.instance.time;
    }
}