import MyTimeline from "../timeline/MyTimeline";
import MyTrack from "../timeline/MyTrack";
import MyClip from "../timeline/MyClip";
import TimelinePane from "../timeline/TimelinePane";
import ActionList from "./ActionList";
import ClipPreview from "./ClipPreview";
import Properties from "./Properties";
import StateManager from "../StateManager";
import ContextMenu, { ContextMenuEvent } from "../contextmenu";

export default class InfoPane extends EventTarget {
    static instance: InfoPane;
    
    element: HTMLElement;
    elements: {[element: string]: HTMLElement} = {};

    properties: Properties;
    clipPreview: ClipPreview;
    actionList: ActionList;

    constructor() {
        super();
        
        InfoPane.instance = this;

        this.element = document.querySelector(".info") as HTMLElement;

        this.properties = new Properties(this.element.querySelector(`.info__content.row[data-content="properties"]`) as HTMLElement);
        this.clipPreview = new ClipPreview(this.element.querySelector(`.info__content.row[data-content="clip"]`) as HTMLElement);
        this.actionList = new ActionList(this.element.querySelector(`.info__content.row[data-content="actions"] .action-list`) as HTMLElement);
    }

    setupEvents() {
        this.properties.setupEvents();
        this.clipPreview.setupEvents();
        this.actionList.setupEvents();

        document.addEventListener("click", (event: Event) => this.onClick(event as CustomEvent));
        this.element.addEventListener("input", (event: Event) => window.requestAnimationFrame(() => this.onInput(event))); // listens to events generated/updated by Actions

        window.addEventListener("message", (event) => {
            const data = event.data;

            switch (event.data.type) {
                // get the Action we are updating via iFrame
                case "updateActionRectAndMatrix": {
                    if (this.updateActionRectAndMatrix(parseInt(data.actionId), data.rect, data.matrix)) {
                        StateManager.instance.record();
                    }
                    break;
                }
                // ...or the Group
                case "updateGroupRectAndMatrix": {
                    if (this.updateGroupRectAndMatrix(parseInt(data.groupId), data.rect, data.matrix)) {
                        StateManager.instance.record();
                    }
                    break;
                }
            }
        });
    }

    onClick(event: Event) {
        const element = event.target as HTMLElement;

        // was it a tab button?
        if (element instanceof HTMLButtonElement && element.closest(".info__tabs")) {
            // hide the others
            Array.from(this.element.querySelectorAll(".info__tabs button")).forEach(b => b.classList.remove("button-primary"));
            Array.from(this.element.querySelectorAll(".info__content")).forEach(c => c.classList.remove("show"));

            // mark the current selected
            this.element.querySelector(`.info__content[data-content="${element.dataset.tab}"]`)!.classList.add("show");
            element.classList.add("button-primary");

            // move it back up
            this.element.scrollTo({ top: 0, behavior: "smooth" });
        }
        // clicked on the add Action
        else if (element.classList.contains("add-action")) {
            // show the context menu
            ContextMenu.instance.dispatch(ContextMenuEvent.Selected, event as MouseEvent);
        }
    }

    // update the Action List title's
    onInput(event: Event) {
        if (!this.selectedTrack) {
            return;
        }
        this.actionList.updateTitles(this.selectedTrack.getActions());
    }

    updateActionRectAndMatrix(actionId: number, rect: DOMRect, matrix: DOMMatrix) {
        let found = false;

        MyTimeline.instance.tracks.slice(0).reverse().forEach(track => {
            if (found) {
                return;
            }

            // find all Actions before our current time
            const actions = track.actions.slice(0).reverse().filter(action => action.startTime <= this.time);

            for (let i = 0; i < actions.length && !found; i++) {
                let action = actions[i] as any;

                // updating Action, or updating a for a Tween (uses otherId property)
                // this is why we reversed the array to work backwards what we should update
                if (action.id === actionId || (Array.isArray(action.otherIds) && action.otherIds.indexOf(actionId) >= 0)) {
                    const theAction = MyTimeline.instance.getAction(action.id);
                    // does it exist?
                    // does the `setRectAndMatrix` exist on the Action?
                    // execute it, if it returns `true` it updated itself
                    if (theAction && "setRectAndMatrix" in theAction && (theAction as any).setRectAndMatrix(rect, matrix)) {

                        // update it's options
                        this.actionList.updateActionOptions(theAction);

                        found = true;
                    }
                }
            }
        });

        return found;
    }

    updateGroupRectAndMatrix(groupId: number, rect: DOMRect, matrix: DOMMatrix) {
        let found = false;

        MyTimeline.instance.tracks.slice(0).reverse().forEach(track => {
            if (found) {
                return;
            }

            const groups = track.groups.slice(0).reverse().filter(group => group.startTime <= this.time);

            for (let i = 0; i < groups.length && !found; i++) {
                let group = groups[i] as any;

                if (group.id === groupId) {
                    const theGroup = MyTimeline.instance.getGroup(group.id);

                    if (theGroup && "setRectAndMatrix" in theGroup && (theGroup as any).setRectAndMatrix(rect, matrix)) {
                        found = true;
                    }
                }
            }
        });

        return found;
    }

    get selectedTrack() {
        return <MyTrack> MyTimeline.instance.selectedTrack;
    }
    get selectedClip() {
        return <MyClip> this.selectedTrack?.selectedClip;
    }
    get time() {
        return TimelinePane.instance.time;
    }
}