import App, { AppEvent } from "../App";
import StagePane from "../stage/StagePane";
import File from "./File";
import Edit from "./Edit";
import Tags from "./Tags";

export default class MenubarPane extends EventTarget {
    static instance: MenubarPane;

    element: HTMLElement;
    elements: {[element: string]: HTMLElement} = {};

    file: File;
    edit: Edit;
    tags: Tags;

    constructor() {
        super();

        MenubarPane.instance = this;

        this.element = document.querySelector("#menubar.pane") as HTMLElement;

        this.file = new File(this.element.querySelector(".file-menu") as HTMLElement);
        this.edit = new Edit(this.element.querySelector(".edit-menu") as HTMLElement);
        this.tags = new Tags(this.element.querySelector(".tags-menu") as HTMLElement);

        this.onSetProject();
    }

    setupEvents() {
        this.element.addEventListener("click", (event) => this.onClick(event));

        this.file.setupEvents();
        this.edit.setupEvents();
        this.tags.setupEvents();

        App.instance.addEventListener(AppEvent.SetProject, (event) => this.onSetProject(event as CustomEvent));
    }

    onClick(event: Event) {
        const element = event.target as HTMLElement;
        const stageAlignment = (this.element.querySelector(`input[name="stage-alignment"]`) as HTMLInputElement).checked;

        // alignment
        if (element.className.indexOf("align-") > -1) {
            const alignment = element.classList.item(0) || "";
            StagePane.instance.post({ type: "alignment", stageAlignment: stageAlignment, alignment: alignment });
        }
        // distrubute width/height
        else if (element.className.indexOf("more-") > -1) {
            StagePane.instance.post({ type: "distribute", stageAlignment: stageAlignment, direction: element.className.indexOf("horiz") > -1 ? "width" : "height" });
        }
    }

    onSetProject(event: CustomEvent | null = null) {
        const project = event ? event.detail : {};
        
        // right side
        (this.element.querySelector(".project-info") as HTMLElement).innerHTML = project.lti ? `${project.lti} ${project.name ? `(${project.name})` : ""}` : "Untitled";
    }
}