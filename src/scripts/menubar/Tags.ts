import { endPoint, fetchWithTimeout } from "../APIConnect";
import Modal from "../Modal";
import StagePane from "../stage/StagePane";
import MyTimeline from "../timeline/MyTimeline";
import TimelinePane from "../timeline/TimelinePane";

export default class Tags extends EventTarget {
    element: HTMLElement;
    menuContent: HTMLElement;

    constructor(element: HTMLElement) {
        super();

        this.element = element;
        this.menuContent = <HTMLElement> this.element.querySelector(".item-menu__content");
    }

    setupEvents() {
        this.element.querySelector("button")?.addEventListener("click", (event) => this.onClick(event));
    }

    onClick(event: Event) {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        Modal.instance.show("Tags", `
            <div class="row">
                <div class="two columns">
                    Tag
                </div>
                <div class="ten columns">
                    <input list="tag-list" name="tag" class="u-full-width" />
                    <datalist id="tag-list"></datalist>
                </div>
            </div>
            <div class="row">
                <div class="two columns">
                    &nbsp;
                </div>
                <div class="ten columns">
                    <button class="button-green add">ADD</button>
                </div>
            </div>
        `);

        const inputTag = Modal.instance.elements.content.querySelector("input")!;
        inputTag.addEventListener("input", async () => {

            // do the request, get the text response
            const optionHTML = await fetchWithTimeout(`${endPoint}tdl?t=${encodeURIComponent(inputTag.value.trim())}`).then(response => response.text());

            // append the text/html to the dataset tag
            Modal.instance.elements.content.querySelector(`datalist#tag-list`)!.innerHTML = optionHTML;
        });
        inputTag.focus();

        Modal.instance.elements.content.querySelector("button")?.addEventListener("click", async () => {
            const value = inputTag.value.trim();
            if (value === "") {
                alert("Tag cannot be empty!");
                return;
            }

            let request = true;

            // check if it exists
            Array.from(this.menuContent.querySelectorAll("div.item")).forEach(item => {
                if ((item as HTMLElement).dataset.name === value) {
                    request = false;
                }
            });

            if (request) {
                this.loadTag(value);

                // good job
                alert(`Added "${value}"`);

                // empty
                inputTag.value = "";
                inputTag.focus();
            }
        });
    }

    async loadTags(tags: Array<string> = []) {
        await Promise.all(tags.map(async (tag: string) => {
            await this.loadTag(tag);
        }));
    }

    async loadTag(tag: string) {
        // if it's already added, we don't need to do a request
        if (!MyTimeline.instance.loader.addTag(tag)) {
            return;
        }

        // append to menu content
        if (!this.menuContent.querySelector(`[data-name="${tag}"]`)) {
            const div = document.createElement("div");
            div.classList.add("item-menu__item");
            div.dataset.name = tag;
            div.innerText = tag;
            this.menuContent.appendChild(div);
        }

        // get the response
        const data = await fetchWithTimeout(`${endPoint}tag/find?t[]=${encodeURIComponent(tag)}`).then(response => response.json());

        if (data.sounds) {
            MyTimeline.instance.loader.addResource("sounds", data.sounds);

            // prevents objects from being passed
            const soundResources = JSON.parse(JSON.stringify(MyTimeline.instance.loader.resources.sounds));

            StagePane.instance.post({
                type: "addResource",
                name: "sounds",
                resources: soundResources
            });
        }

        if (data.images) {
            MyTimeline.instance.loader.addResource("images", data.images);

            // prevents objects from being passed
            const imageResources = JSON.parse(JSON.stringify(MyTimeline.instance.loader.resources.images));

            StagePane.instance.post({
                type: "addResource",
                name: "images",
                resources: imageResources
            });
        }
    }
}