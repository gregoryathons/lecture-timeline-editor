import Hotkeys from "@teachingtextbooks/hotkeys";
import { ActionType } from "lecture-timeline";
import StagePane, { StagePaneEvent } from "../stage/StagePane";
import StateManager, { StateManagerEvent } from "../StateManager";
import MyAction, { findOpening } from "../timeline/MyAction";
import MyTimeline from "../timeline/MyTimeline";
import MyTrack from "../timeline/MyTrack";
import MyClip from "../timeline/MyClip";
import TimelinePane from "../timeline/TimelinePane";

export default class Edit extends EventTarget {
    element: HTMLElement;
    elements: {[element: string]: HTMLElement} = {};

    lastCommand: string = "";
    copiedActions: Array<string> = [];
    tempActionIds: Array<number> = [];
    tempGroupIds: Array<number> = [];

    constructor(element: HTMLElement) {
        super();

        this.element = element;

        this.elements.indexInput = this.element.querySelector(`.item-menu__item.index input`) as HTMLElement;
        this.elements.indexLength = this.element.querySelector(`.item-menu__item.index span.length`) as HTMLElement;
    }

    setupEvents() {
        this.element.addEventListener("click", (event) => this.onClick(event));
        this.elements.indexInput.addEventListener("keyup", (event) => this.onInteraction(event));

        StateManager.instance.addEventListener(StateManagerEvent.Updated, (event: Event) => this.onStateManager(event));
        StateManager.instance.addEventListener(StateManagerEvent.Undo, (event: Event) => this.onStateManager(event));
        StateManager.instance.addEventListener(StateManagerEvent.Redo, (event: Event) => this.onStateManager(event));

        StagePane.instance.addEventListener(StagePaneEvent.UpdatedDisplay, (event: Event) => this.onStagePane(event));

        Hotkeys.on("control+z", () => StagePane.instance.post({ type: "edit-undo" }));
        Hotkeys.on("control+shift+z", () => StagePane.instance.post({ type: "edit-redo" }));
        Hotkeys.on("control+y", () => StagePane.instance.post({ type: "edit-redo" }));
        Hotkeys.on("control+c", () => StagePane.instance.post({ type: "edit-copy" }));
        Hotkeys.on("control+v", () => StagePane.instance.post({ type: "edit-paste" }));
        Hotkeys.on("control+g", () => StagePane.instance.post({ type: "edit-group" }));
        Hotkeys.on("control+shift+g", () => StagePane.instance.post({ type: "edit-ungroup" }));

        window.addEventListener("message", (event) => {
            switch (event.data.type) {
                case "edit-undo": {
                    this.undo();
                    break;
                }
                case "edit-redo": {
                    this.redo();
                    break;
                }
                case "edit-copy": {
                    this.copyActions(event.data);
                    break;
                }
                case "edit-paste": {
                    this.pasteActions();
                    break;
                }
                case "edit-delete": {
                    this.deleteActions(event.data);
                    break;
                }
            }
        });
    }

    onClick(event: Event) {
        const element = event.target as HTMLElement;

        if (element.classList.contains("undo")) {
            StagePane.instance.post({ type: "edit-undo" });
        }
        else if (element.classList.contains("redo")) {
            StagePane.instance.post({ type: "edit-redo" });
        }
        else if (element.classList.contains("copy")) {
            StagePane.instance.post({ type: "edit-copy" });
        }
        else if (element.classList.contains("paste")) {
            StagePane.instance.post({ type: "edit-paste" });
        }
        else if (element.classList.contains("group")) {
            StagePane.instance.post({ type: "edit-group" });
        }
        else if (element.classList.contains("ungroup")) {
            StagePane.instance.post({ type: "edit-ungroup" });
        }
    }

    onInteraction(event: Event) {
        if (event.target === this.elements.indexInput) {
            // user hit enter
            if ((event as KeyboardEvent).key.toLowerCase() === "enter") {
                const want = parseInt((this.elements.indexInput as HTMLInputElement).value);
                // is it within range
                if (want >= 1 && want <= parseInt(this.elements.indexLength.innerText)) {
                    StateManager.instance.jumpTo(want - 1);
                }
            }
        }
    }

    onStateManager(event: Event) {
        const data = (event as CustomEvent).detail;

        (this.elements.indexInput as HTMLInputElement).value = (data.index + 1).toString();
        this.elements.indexLength.innerText = data.length;
    }

    // usually to post to the iFrame to reselect the items
    onStagePane(event: Event) {
        switch (this.lastCommand) {
            case "pastActions": {
                StagePane.instance.post({
                    type: "edit-paste-complete",
                    actionIds: this.tempActionIds
                });
                break;
            }
        }

        this.lastCommand = "";
        this.tempActionIds.length = this.tempGroupIds.length = 0;
    }

    async undo() {
        if (!TimelinePane.instance.isPlaying) {
            await StateManager.instance.undo();
        }
    }

    async redo() {
        if (!TimelinePane.instance.isPlaying) {
            await StateManager.instance.redo();
        }
    }

    copyActions(data: any) {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        // empty first group
        this.copiedActions.length = 0;

        // export each Action
        (data.actionIds as Array<number>).forEach(actionId => {
            const action = MyTimeline.instance.getAction(actionId);
            if (action) {
                const data = (action as any).export();
                delete data.id; // don't copy this
                this.copiedActions.push(JSON.stringify(data));
            }
        });
    }

    async pasteActions() {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        this.lastCommand = "pasteActions";
        const newActionIds: Array<number> = [];

        this.copiedActions.forEach(copiedAction => {
            const actionData = JSON.parse(copiedAction);

            // re-adjust some props
            actionData.startTime = this.time;
            actionData.clipId = this.selectedClip?.id || 0;

            // finds an opening
            findOpening(MyTimeline.instance, actionData.rect);

            // create the MyAction
            const type = actionData.type as keyof typeof ActionType;
            const myAction = new MyAction(new ActionType[type](actionData));
            newActionIds.push(myAction.id);

            // add to the Track
            this.selectedTrack.addAction(myAction);
        });

        this.tempActionIds = newActionIds;
    }

    deleteActions(data: any) {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        // delete each Action
        (data.actionIds as Array<number>).forEach(actionId => {
            const action = MyTimeline.instance.getAction(actionId);
            if (action) {
                this.selectedTrack.removeAction((action as any).editorRef);
            }
        });
    }

    get selectedTrack() {
        return <MyTrack> MyTimeline.instance.selectedTrack;
    }
    get selectedClip() {
        return <MyClip> this.selectedTrack?.selectedClip;
    }
    get time() {
        return TimelinePane.instance.time;
    }
}