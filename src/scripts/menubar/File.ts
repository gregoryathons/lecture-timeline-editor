import Hotkeys from "@teachingtextbooks/hotkeys";
import App, { AppEvent } from "../App";
import Modal from "../Modal";
import StateManager from "../StateManager";
import MyTimeline from "../timeline/MyTimeline";
import MyTrack from "../timeline/MyTrack";
import TimelinePane from "../timeline/TimelinePane";

export default class File extends EventTarget {
    element: HTMLElement;

    constructor(element: HTMLElement) {
        super();

        this.element = element;
    }

    setupEvents() {
        this.element.addEventListener("click", (event) => this.onClick(event));

        App.instance.addEventListener(AppEvent.Start, () => this.startNew());

        Hotkeys.on("control+n", () => this.commandNew());
        Hotkeys.on("control+o", () => this.commandOpen());
        Hotkeys.on("control+s", () => this.commandSave());
        Hotkeys.on("control+s+shift", () => this.showSaveInfo());

        window.addEventListener("message", (event: MessageEvent) => {
            switch (event.data.type) {
                case "file-new": {
                    this.commandNew();
                    break;
                }
                case "file-open": {
                    this.commandOpen();
                    break;
                }
                case "file-save": {
                    this.commandSave();
                    break;
                }
                case "file-save-as": {
                    this.showSaveInfo();
                    break;
                }
            }
        });
    }

    onClick(event: Event) {
        const element = event.target as HTMLElement;

        // new
        if (element.classList.contains("new")) {
            this.commandNew();
        }
        // open
        else if (element.classList.contains("open")) {
            this.commandOpen();
        }
        // save
        else if (element.classList.contains("save")) {
            this.commandSave();
        }
        // save as
        else if (element.classList.contains("save-as")) {
            this.showSaveInfo();
        }
    }

    commandNew() {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        if (StateManager.instance.states.length) {
            if (confirm("Start a new Lecture?")) {
                this.startNew();
            }
        }
        else {
            this.startNew();
        }
    }

    commandOpen() {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        App.instance.getTimelines().then((data: any) => {
            // sort by date
            data.sort((a: any, b: any) => {
                const da = new Date(a.lastmod);
                const db = new Date(b.lastmod);
                return db.getTime() - da.getTime();
            });

            // output display in a table
            let html = `
                <div class="container">
                    <input type="text" name="search" class="u-full-width" placeholder="Search LTI/Name" autocomplete="off" />
                </div>
                <table class="u-full-width">
            `;

            data.forEach((item: any) => {
                const date = new Date(item.lastmod);
                const lti = item.lti.trim();
                const name = item.name.trim();

                // set the dataset
                html += `
                    <tr data-lti="${lti}" data-name="${name}">
                        <td>
                            ${lti} ${name !== "" ? `(${name})` : ""}
                        </td>
                        <td>
                            ${date.toLocaleString(undefined, { year: "numeric", month: "long", day: "numeric", hour: "numeric", minute: "numeric" })}
                        </td>
                    </tr>
                `;
            });

            // show it
            Modal.instance.show("Open", html + "</table>");

            const tableTRs = Modal.instance.elements.content.querySelectorAll("tr");

            // search
            const inputSearch = <HTMLInputElement> Modal.instance.elements.content.querySelector(`input[name="search"]`)!;
            inputSearch.addEventListener("input", () => {
                const value = inputSearch.value.toString().trim();
                Array.from(tableTRs).forEach(tr => {
                    const lti = tr.dataset.lti!.toLowerCase().indexOf(value) >= 0;
                    const name = tr.dataset.name!.toLowerCase().indexOf(value) >= 0;
                    tr.hidden = !(lti || name);
                });
            });
            inputSearch.focus();

            // apply events
            Array.from(tableTRs).forEach(tr => {
                tr.addEventListener("mouseover", (event: MouseEvent) => {
                    tr.classList.remove("delete", "load");

                    if (event.altKey && event.shiftKey) {
                        tr.classList.add("delete");
                    }
                    else if (event.shiftKey) {
                        tr.classList.add("load");
                    }
                });

                // load this LTI when clicking on the table row
                tr.addEventListener("click", (event: MouseEvent) => {
                    const altKey = event.altKey;
                    const shiftKey = event.shiftKey;
                    const lti = tr.dataset.lti!;
                    const name = tr.dataset.name!;

                    // remove it!!!?
                    if (altKey && shiftKey) {
                        if (confirm(`Are you sure you want to REMOVE:\n\n${lti} ${name ? `(${name})` : ""}`)) {
                            Modal.instance.hide();

                            App.instance.removeTimeline(lti);
                        }
                    }
                    // load it
                    else {
                        Modal.instance.hide();

                        // set the proejct LTI
                        if (!shiftKey) {
                            App.instance.setProject(lti, name);
                        }

                        // load the Timeline, if shiftKey is held down then it won't import the data, just return it
                        App.instance.loadTimeline(lti, !shiftKey).then(data => {
                            console.log("LTI Info:", lti, name, "Timeline:", data);
                        });
                    }
                });
            });
        });
    }

    commandSave() {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        if (App.instance.project.lti === "") {
            this.showSaveInfo();
        }
        else {
            this.saveTimeline();
        }
    }

    startNew() {
        App.instance.setProject("", "");

        // empty
        StateManager.instance.index = 0;
        StateManager.instance.states.length = 0;

        // set the MAIN track
        MyTimeline.instance.empty();
        MyTimeline.instance.addTrack(new MyTrack({ name: "MAIN" }));

        // start it
        TimelinePane.instance.drawer.dispatchEvent(new CustomEvent("updateTime"));
    }

    showSaveInfo() {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        Modal.instance.show("Save", `
            <div class="row">
                <div class="two columns">
                    LTI
                </div>
                <div class="ten columns">
                    <input name="lti" class="u-full-width" value="${App.instance.project.lti}" style="text-transform:uppercase" />
                </div>
            </div>
            <div class="row">
                <div class="two columns">
                    Name
                </div>
                <div class="ten columns">
                    <input name="name" class="u-full-width" value="${App.instance.project.name}" />
                </div>
            </div>
            <div class="row">
                <div class="two columns">
                    &nbsp;
                </div>
                <div class="ten columns">
                    <button class="button button-green save">SAVE</button>
                </div>
            </div>
        `);

        const inputs = Modal.instance.elements.content.querySelectorAll("input");

        // focus on first input
        inputs[0].focus();

        // apply events
        Modal.instance.elements.content.querySelector("button")?.addEventListener("click", () => {    
            // make sure it isn't empty
            const value = inputs[0].value.trim();
            if (value === "") {
                alert("LTI cannot be empty!");
                return;
            }

            // set the project LTI and name
            App.instance.setProject(value.toUpperCase(), inputs[1].value.trim());

            // done with it
            Modal.instance.hide();

            // save it
            this.saveTimeline();
        });
    }

    saveTimeline() {
        App.instance.saveTimeline({
            time: this.time,
            timeline: MyTimeline.instance.export(),
            trackId: MyTimeline.instance.selectedTrack?.id
        });
    }

    get time() {
        return TimelinePane.instance.time;
    }
}