import Loader from "./Loader";
import Modal from "./Modal";

export const endPoint = "https://tthq.me/api/";

export async function fetchWithTimeout(url: string, options: any = {}) {    
    const abortController = new AbortController();

    const timer = setTimeout(() => {
        abortController.abort();
        Loader.instance.onLoaderStop();
        Modal.instance.show(`Error <span class="material-icons u-pull-right">sentiment_very_dissatisfied</span>`, `Sorry, there was an error connecting to the TTHQ endpoint`);
    }, 8 * 1000);

    const response = await fetch(url, {
        ...options,
        signal: abortController.signal  
    });

    clearTimeout(timer);
    
    return response;
}

export default class API {
    static token = "fFbAQzFRD1UruqPedvXql4tH";

    // create OR modify existing
    static async saveItem(data: any) {
        return await fetchWithTimeout(`${endPoint}dv/endpointX/`, {
            method: "post",
            headers: new Headers({
                Authorization: API.token,
            }),
            body: JSON.stringify(data)
        })
        .then(response => response.json());
    }

    // retrieves a list of items
    static async getItems() {
        return await fetchWithTimeout(`${endPoint}dv/listX/`, {
            method: "get",
            headers: new Headers({
                Authorization: API.token
            })
        })
        .then(response => response.json());
    }

    // retrieves singular item data
    static async loadItem(lti: string) {
        return await fetchWithTimeout(`${endPoint}dv/dataX/${lti}`, {
            method: "get",
            headers: new Headers({
                Authorization: API.token,
            })
        }).then(response => response.json());
    }

    // removes a singular item
    static async deleteItem(lti: string) {
        return await fetchWithTimeout(`${endPoint}dv/deleteX/${lti}`, {
            method: "delete",
            headers: new Headers({
                Authorization: API.token
            })
        })
        .then(response => response.json());
    }
}