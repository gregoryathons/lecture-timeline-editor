import Loader from "./Loader";
import MyTimeline from "./timeline/MyTimeline";
import MyTrack from "./timeline/MyTrack";
import StagePane from "./stage/StagePane";
import InfoPane from "./info/InfoPane";
import TimelinePane from "./timeline/TimelinePane";
import StateManager from "./StateManager";
import MenubarPane from "./menubar/MenubarPane";
import Modal from "./Modal";
import API from "./APIConnect";
import "./contextmenu/index.ts";
// const pack = require("../../package.json");

interface IProject {
    lti: string;
    name: string;
};

export const AppEvent = {
    Start: "start",
    FetchStart: "fetch-start",
    FetchStop: "fetch-stop",
    SetProject: "set-project"
};

export default class App extends EventTarget {
    static instance: App;

    // current project LTI and nickname
    project = {
        lti: "",
        name: ""
    } as IProject;

    loader: Loader;
    timeline: MyTimeline;
    menubarPane: MenubarPane;
    stagePane: StagePane;
    infoPane: InfoPane;
    timelinePane: TimelinePane;
    stateManager: StateManager;
    modal: Modal;

    // last save state index
    saveStateIndex = 0;

    constructor() {
        super();

        App.instance = this;

        // this is ugly, but I wanted to have a loader splash upon entry
        this.loader = new Loader();
        this.timeline = new MyTimeline();
        this.stagePane = new StagePane();
    }

    // call `setupEvents` on all our classes
    setupEvents() {
        // this.loader = new Loader();
        // this.timeline = new MyTimeline();
        this.menubarPane = new MenubarPane();
        // this.stagePane = new StagePane();
        this.infoPane = new InfoPane();
        this.timelinePane = new TimelinePane();
        this.stateManager = new StateManager();
        this.modal = new Modal();

        // setup the events
        this.loader.setupEvents();
        this.timeline.setupEvents();
        this.menubarPane.setupEvents();
        this.stagePane.setupEvents();
        this.infoPane.setupEvents();
        this.timelinePane.setupEvents();
        this.stateManager.setupEvents();
        this.modal.setupEvents();

        // check if the admin wants to leave, cross check the indexes values
        window.addEventListener("beforeunload", (event: Event) => {
            if (this.saveStateIndex !== this.stateManager.index) {
                event.returnValue = true;
            }
        });
    }

    // init the app
    async init(templateURL: string) {
        // load the template
        this.stagePane.loadTemplate(templateURL, () => {
            // now show the main
            (document.querySelector("main") as HTMLElement).hidden = false;

            // tell everyone to setup their events
            this.setupEvents();

            // set the root
            this.stagePane.post({ 
                type: "setLoaderRootPath",
                rootPath: this.timeline.loader.rootPath
            });

            // set the placeholder
            this.timeline.loader.addResource("images", [{ name: "PLACEHOLDER", title: "PLACEHOLDER", kind: "svg", path: "gregory/TT.G3A/placeholder.svg" }] as any);

            // prevents objects from being passed
            const imageResources = JSON.parse(JSON.stringify(this.timeline.loader.resources.images));

            this.stagePane.post({
                type: "addResource",
                name: "images",
                resources: imageResources
            });

            this.dispatchEvent(new CustomEvent(AppEvent.Start));
        });
    }

    // save
    async saveTimeline(data: any) {
        // start the fetching
        this.dispatchEvent(new CustomEvent(AppEvent.FetchStart));

        // remember the state index
        this.saveStateIndex = this.stateManager.index;

        // save it
        const res = await API.saveItem({
            metadata: this.project,
            data: data
        });

        this.dispatchEvent(new CustomEvent(AppEvent.FetchStop));

        return res;
    }

    // load item
    async loadTimeline(lti: string, doImport: boolean = true) {
        this.dispatchEvent(new CustomEvent(AppEvent.FetchStart));

        let res;

        // it was from the StateManager
        try {
            res = JSON.parse(lti);
        }
        // passed a LTI string
        catch (e) {
            res = await API.loadItem(lti);
        }

        if (doImport) {
            await this._loadTimeline(res);
        }
        
        this.dispatchEvent(new CustomEvent(AppEvent.FetchStop));

        return res;
    }

    private async _loadTimeline(data: any) {
        // this is because it "might" have been passed through the StateManager
        const timeline = typeof data.timeline === "string" ? JSON.parse(data.timeline) : data.timeline;

        // import the resources
        await this.menubarPane.tags.loadTags(timeline.tags);

        // import it
        await this.timeline.import(timeline);

        // set the selected Track from last save
        this.timeline.selectedTrack = this.timeline.getTrack(data.trackId || 1) as MyTrack;

        // sync our local timeline to the iFrame's
        await this.stagePane.updateDisplay(true, false);

        // set the time and adjust the bounds
        this.timelinePane.drawer.onResize();

        // show the current situation along the Timeline
        this.timelinePane.time = data.time;

        // scoot over
        this.timelinePane.drawer.onTimelineUpdate(data.time);
    }

    // get all the timelines saved
    async getTimelines() {
        this.dispatchEvent(new CustomEvent(AppEvent.FetchStart));

        const res = await API.getItems();

        this.dispatchEvent(new CustomEvent(AppEvent.FetchStop));

        return res;
    }

    // remove the timeline
    async removeTimeline(lti: string) {
        this.dispatchEvent(new CustomEvent(AppEvent.FetchStart));

        const res = await API.deleteItem(lti);

        this.dispatchEvent(new CustomEvent(AppEvent.FetchStop));

        return res;
    }

    setProject(lti: string, name: string) {
        this.project.lti = lti;
        this.project.name = name;
        this.dispatchEvent(new CustomEvent(AppEvent.SetProject, { detail: this.project }));
    }
}