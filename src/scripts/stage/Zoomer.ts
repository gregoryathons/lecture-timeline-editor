import StagePane from "./StagePane";

// help from: 
// https://jsfiddle.net/Pimigo/sy9c8z5q/8/

export default class Zoomer {
    element: HTMLElement;

    cssTransition = "transform 0.3s";

    pos = new DOMPoint();

    zoom = 1;
    zoomMin = 1;
    zoomMax = 10;
    zoomPt = new DOMPoint();
    zoomTarget = new DOMPoint();

    _isKeyDown = false;
    isMouseDown = false;
    mousePt = new DOMPoint();

    set isKeyDown(value) {
        this._isKeyDown = value;

        const iframeBlocker = StagePane.instance.elements.iframeBlocker;
        iframeBlocker.classList.remove("zoomer-move");
        if (value) {
            iframeBlocker.classList.add("zoomer-move");
        }
    }
    get isKeyDown() {
        return this._isKeyDown;
    }

    constructor(element: HTMLElement) {
        this.element = element;

        this.element.style.transition = this.cssTransition;
    }

    setupEvents() {
        window.addEventListener("keydown", (event: KeyboardEvent) => this.onKey({ isKeyDown: event.key === " " ? true : false }));
        window.addEventListener("keyup", (event: KeyboardEvent) => this.onKey({ isKeyDown: false }));

        window.addEventListener("mousemove", (event: MouseEvent) => this.onMouse(event));
        window.addEventListener("mousedown", (event: MouseEvent) => this.onMouse(event));
        window.addEventListener("mouseup", (event: MouseEvent) => this.onMouse(event));

        window.addEventListener("message", (event: MessageEvent) => {
            switch (event.data.type) {
                case "zoomer-wheel": {
                    this.onWheel(event.data);
                    break;
                }
                case "zoomer-key": {
                    this.onKey(event.data);
                    break;
                }
            }
        });
    }

    onWheel(data: any) {
        let factor = 0.5;

        const offset = this.element.getBoundingClientRect()
		this.zoomPt.x = data.pageX - offset.left;
		this.zoomPt.y = data.pageY - offset.top;

        // determine the point on where target is zoom in
        this.zoomTarget.x = (this.zoomPt.x - this.pos.x) / this.zoom;
        this.zoomTarget.y = (this.zoomPt.y - this.pos.y) / this.zoom;

        // apply zoom
        this.zoom += (data.deltaY * 0.01) * factor * this.zoom;
        this.zoom = Math.max(this.zoomMin, Math.min(this.zoomMax, this.zoom));

        // calculate x and y
        this.pos.x = -this.zoomTarget.x * this.zoom + this.zoomPt.x;
        this.pos.y = -this.zoomTarget.y * this.zoom + this.zoomPt.y;

        this.update();
    }

    onKey(data: any) {
        this.isKeyDown = StagePane.instance.isBlocking = data.isKeyDown;

        if (!this.isKeyDown) {
            this.stopPan();
        }
    }

    onMouse(event: MouseEvent) {
        if (!this.isKeyDown) {
            return;
        }

        switch (event.type) {
            case "mousedown": {
                this.isMouseDown = true;
                this.mousePt.x = event.pageX;
                this.mousePt.y = event.pageY;
                this.element.style.transition = "";
                break;
            }
            case "mousemove": {
                if (this.isMouseDown) {
                    let x = event.pageX - this.mousePt.x;
                    let y = event.pageY - this.mousePt.y;

                    this.mousePt.x = event.pageX;
                    this.mousePt.y = event.pageY;

                    this.pos.x += x;
                    this.pos.y += y;

                    this.update();
                }
                break;
            }
            case "mouseup": {
                this.stopPan();
                break;
            }
        }

        this.update();
    }

    stopPan() {
        this.isMouseDown = false;
        this.element.style.transition = this.cssTransition;
    }

    update() {
        const w = this.element.offsetWidth;
        const h = this.element.offsetHeight;

        if (this.pos.x > 0) this.pos.x = 0;
        if (this.pos.x + w * this.zoom < w) this.pos.x = -w * (this.zoom - 1);
        if (this.pos.y > 0) this.pos.y = 0;
        if (this.pos.y + h * this.zoom < h) this.pos.y = -h * (this.zoom - 1);

        this.element.style.transform = `translate(${this.pos.x}px, ${this.pos.y}px) scale(${this.zoom})`;
    }
}