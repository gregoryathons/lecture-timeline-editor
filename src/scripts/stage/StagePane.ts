import { TimelineEvent } from "lecture-timeline";
import InfoPane from "../info/InfoPane";
import MyTimeline from "../timeline/MyTimeline";
import TimelinePane from "../timeline/TimelinePane";
import { clamp, debounce, trace } from "../utils";
import StateManager from "../StateManager";
import Modal from "../Modal";
import Zoomer from "./Zoomer";

interface IDragInfo {
    canDragUp: boolean;
    isDragUp: boolean;
    canDragLeft: boolean;
    isDragLeft: boolean;
};

export const StagePaneEvent = {
    UpdatedDisplay: "updated-display",
    Resize: "resize"
};

export default class StagePane extends EventTarget {
    static instance: StagePane;

    zoomer: Zoomer;

    menubarHeight = 28;
    minWidth = 240; // for the Info Pane
    minHeight = 100; // for the Timeline Pane

    contentWindow: Window;

    element: HTMLElement;
    elements: {[element: string]: HTMLElement} = {};

    dragInfo: IDragInfo = <IDragInfo> {};

    constructor() {
        super();

        StagePane.instance = this;

        this.element = document.querySelector("#stage.pane") as HTMLElement;

        this.elements.iframe = this.element.querySelector("iframe") as HTMLElement;
        this.elements.iframeBlocker = this.element.querySelector("#iframe-blocker") as HTMLElement;

        this.zoomer = new Zoomer(this.elements.iframe);
    }

    setupEvents() {
        this.zoomer.setupEvents();

        document.addEventListener("mousedown", (event) => this.onDocumentInteraction(event as MouseEvent));
        document.addEventListener("mouseup", (event) => this.onDocumentInteraction(event as MouseEvent));
        document.addEventListener("mousemove", (event) => this.onDocumentInteraction(event as MouseEvent));

        this.elements.iframe.addEventListener("mouseenter", (event) => this.onDocumentInteraction(event as MouseEvent));

        // a nice debouce
        const ftnUpdateDisplay = debounce(() => this.updateDisplay(true));

        // any input field that is updated, we update the stage
        InfoPane.instance.addEventListener("actionUpdate", () => ftnUpdateDisplay());
        InfoPane.instance.actionList.addEventListener("actionsSorted", () => ftnUpdateDisplay());
        InfoPane.instance.actionList.element.addEventListener("input", () => ftnUpdateDisplay());

        // when the admin selects a track or moves around the timeline
        TimelinePane.instance.addEventListener("clipUpdate", () => ftnUpdateDisplay());
        TimelinePane.instance.addEventListener("actionUpdate", () => ftnUpdateDisplay());
        TimelinePane.instance.toolbar.addEventListener("input", () => ftnUpdateDisplay());
        TimelinePane.instance.drawer.addEventListener("updateTime", () => window.requestAnimationFrame(() => {
            this.post({ type: "deselect" });
            this.updateDisplay(); // don't need to call `ftnUpdateDisplay`
        }));

        MyTimeline.instance.addEventListener(TimelineEvent.BookmarkAdded, () => ftnUpdateDisplay());
        MyTimeline.instance.addEventListener(TimelineEvent.BookmarkRemoved, () => ftnUpdateDisplay());
        MyTimeline.instance.addEventListener(TimelineEvent.ClipAdded, () => ftnUpdateDisplay());
        MyTimeline.instance.addEventListener(TimelineEvent.ClipRemoved, () => ftnUpdateDisplay());
        MyTimeline.instance.addEventListener(TimelineEvent.ActionAdded, () => ftnUpdateDisplay());
        MyTimeline.instance.addEventListener(TimelineEvent.ActionRemoved, () => ftnUpdateDisplay());
        MyTimeline.instance.addEventListener("selectedTrack", () => window.requestAnimationFrame(() => this.updateDisplay(true) ));

        // listen for postMesssages
        window.addEventListener("message", (event: MessageEvent) => { });
    }

    loadTemplate(templateURL: string, callback: Function) {
        const iframe = this.elements.iframe as HTMLIFrameElement;

        // set the listener
        this.elements.iframe.addEventListener("load", () => {
            this.contentWindow = iframe.contentWindow as Window;

            // don't need to show it any longer
            this.isBlocking = false;

            // do the callback
            callback();
        }, { once: true });
        
        // set the src, this will load
        iframe.src = templateURL;
    }

    async syncDisplay(win: Window | null = null) {   
        // get the Timeline data
        const data = MyTimeline.instance.export();

        await Promise.all(data.tracks.map(async (track: any) => {
            return Promise.all(track.actions.map(async (action: any) => {
                // if the Action is using a Resource id and no base64 string, load it
                return Promise.all((action.resourceNames || []).map(async (name: string) => {
                    await MyTimeline.instance.loader.loadResource(name);
                }));
            }));
        }));

        await Promise.all(data.tracks.map(async (track: any) => {
            return Promise.all(track.clips.map(async (clip: any) => {
                // if the Clip is using a resource id and no base64 string, load it
                if (clip.resourceName) {
                    await MyTimeline.instance.loader.loadResource(clip.resourceName);
                }
            }));
        }));

        // post to the iFrame the resources we loaded
        Object.keys(MyTimeline.instance.loader.resources).forEach(name => {
            MyTimeline.instance.loader.resources[name].forEach(resource => {
                if (!resource.dataURL) {
                    return;
                }

                // post to the dataURL/base64 string
                this.post({
                    type: "updateResource",
                    name: resource.name,
                    dataURL: resource.dataURL
                }, win);
            });
        });
        
        // post/sync to the iFrame
        this.post({
            type: "import", 
            timeline: data, 
            trackId: MyTimeline.instance.selectedTrack?.id || 0
        }, win);
    }

    async updateDisplay(sync: boolean = false, record: boolean = true) {
        if (TimelinePane.instance.isPlaying) {
            return;
        }
        
        // update the iFrame
        if (sync) {
            await this.syncDisplay();

            // State Manager record
            record && StateManager.instance.record();
        }

        // this keeps our local timeline in sync with the iFrame
        MyTimeline.instance.jumpTo(this.time);

        // tell everyone we updated the display
        this.dispatchEvent(new CustomEvent(StagePaneEvent.UpdatedDisplay));

        // tell the iFrame to jumpTo the time
        this.post({ type: "jumpTo", time: this.time });
    }

    play(win: Window | null = null) {
        this.post({ type: TimelineEvent.Play, time: this.time }, win);
    }

    pause(win: Window | null = null) {
        this.post({ type: TimelineEvent.Pause }, win);
    }

    post(data: any, win: Window | null = null) {
        (win || this.contentWindow).postMessage(data, "*");
    }

    onDocumentInteraction(event: MouseEvent) {
        if (Modal.instance.isShowing) {
            return;
        }

        switch (event.type) {
            case "mousedown": {
                if (event.button) {
                    return;
                }

                this.dragInfo.isDragUp = this.dragInfo.canDragUp;
                this.dragInfo.isDragLeft = this.dragInfo.canDragLeft;

                if (!this.dragInfo.isDragUp && !this.dragInfo.isDragLeft) {
                    return;
                }

                event.preventDefault();

                // show the overlay
                this.isBlocking = true;

                break;
            }
            case "mouseup": {
                if (this.dragInfo.isDragUp || this.dragInfo.isDragLeft) {
                    this.dispatchEvent(new CustomEvent(StagePaneEvent.Resize));
                }
        
                // hide the overlay
                this.isBlocking = false;

                this.dragInfo.isDragLeft = this.dragInfo.isDragUp = false;

                break;
            }
            case "mousemove": case "mouseenter": {
                const mx = event.x;
                const my = event.y;

                // we are adjusting an element out side of this stage, the Main... oh well
                if (this.dragInfo.isDragLeft) {
                    let value = clamp(window.innerWidth - mx, this.minWidth, window.innerWidth - 240);
                    document.querySelector("main")!.style.gridTemplateColumns = `0px 1fr ${value}px`;
                    return;
                }
                if (this.dragInfo.isDragUp) {
                    let value = clamp(window.innerHeight - my, this.minHeight, window.innerHeight - 240);
                    document.querySelector("main")!.style.gridTemplateRows = `${this.menubarHeight}px calc(100vh - ${value}px - ${this.menubarHeight}px) ${value}px`;
                    return;
                }

                const bounds = this.element.getBoundingClientRect();
                const x = Math.abs(bounds.right - mx);
                const y = Math.abs(bounds.bottom - my);

                // assume nothing is draggable
                this.dragInfo.canDragUp = y < 4 && event.x < bounds.right;
                this.dragInfo.canDragLeft = x < 4;

                // change the cursor and dragInfo properties if they are draggable
                const cursor = this.dragInfo.canDragLeft ? "col-resize" : this.dragInfo.canDragUp ? "row-resize" : "auto";
                document.body.style.cursor = cursor;

                break;
            }
        }
    }

    set isBlocking(value: boolean) {
        this.elements.iframeBlocker.hidden = !value;
    }
    get isBlocking() {
        return !this.elements.iframeBlocker.hidden;
    }

    get time() {
        return TimelinePane.instance.time;
    }
}