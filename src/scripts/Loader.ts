// declare global {
//     interface WindowEventMap {
//         "loader-fileLoadEnd": CustomEvent<{}>,
//         "loader-fileLoad": CustomEvent<{}>
//     }
// }

import { TimelineEvent } from "lecture-timeline";
import App, { AppEvent } from "./App";
import MyTimeline from "./timeline/MyTimeline";

export default class Loader {
    static instance: Loader;

    element: HTMLElement;
    elements: {[element: string]: HTMLElement} = {};
    count = 0; // for overlapping

    constructor() {
        Loader.instance = this;

        this.element = document.querySelector(".loader") as HTMLElement;
        this.elements.block = this.element.querySelector(".loader__block") as HTMLElement;
    }

    setupEvents() {
        MyTimeline.instance.loader.addEventListener(TimelineEvent.LoaderStart, (event) => this.onLoaderStart(event as CustomEvent));
        MyTimeline.instance.loader.addEventListener(TimelineEvent.LoaderStop, (event) => this.onLoaderStop(event as CustomEvent));

        App.instance.addEventListener(AppEvent.FetchStart, (event) => this.onLoaderStart(event as CustomEvent));
        App.instance.addEventListener(AppEvent.FetchStop, (event) => this.onLoaderStop(event as CustomEvent));

        App.instance.addEventListener(AppEvent.Start, () => this.onAppStart());
    }

    onAppStart() {
        this.elements.block.hidden = true;

        // remove the splash
        const splash = this.element.querySelector(".loader__splash")!;
        splash.animate([{ opacity: 0 }], { duration: 800 }).addEventListener("finish", () => {
            splash.remove();

            this.element.hidden = true;
        });

        // spinner
        splash.querySelector(".spinner")!.animate([{ transform: "scale(4, 4)" }], { duration: 800, easing: "ease-in" });
    }

    onLoaderStart(event: CustomEvent | null = null) {
        this.count++;
        this.element.hidden = this.elements.block.hidden = false;
    }

    onLoaderStop(event: CustomEvent | null = null) {
        this.count--;
        if (this.count <= 0) {
            this.element.hidden = this.elements.block.hidden = true;
        }
    }
}