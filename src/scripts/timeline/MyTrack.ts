import { Clip, TimelineEvent, Track } from "lecture-timeline";
import MyTimeline from "./MyTimeline";
import MyAction from "./MyAction";
import MyClip from "./MyClip";
import TimelinePane from "./TimelinePane";
import { trace } from "../utils";
import InfoPane from "../info/InfoPane";
import StagePane, { StagePaneEvent } from "../stage/StagePane";
import Bookmark from "./Bookmark";


export default class MyTrack extends Track {

    content: SVGGElement;

    bookmarkRefs: Array<Bookmark> = []; // keeps references to the renderers

    onDrawerEventBound: Function; // so we can remove it later
    onInfoPaneEventBound: Function;
    onSelectedTrackBound: Function;
    onStagePaneEventBound: Function;

    private _selectedClip: MyClip | null = null;

    constructor(data: any | null = null) {
        super(data);

        // our Track group content
        this.content = document.createElementNS("http://www.w3.org/2000/svg", "g");
        this.content.classList.add("track");
        this.content.dataset.id = this.id.toString();

        // keep reference to the bookmarks
        this.bookmarks.forEach(bookmark => {
            const bm = new Bookmark(bookmark);
            this.bookmarkRefs.push(bm);
            this.content.appendChild(bm.content);
        });

        // listen for the time update
        this.onDrawerEventBound = (event: Event) => this.onDrawerEvent(event as CustomEvent);
        TimelinePane.instance.drawer.addEventListener("updateTime", this.onDrawerEventBound as EventListener);
        TimelinePane.instance.drawer.addEventListener("updateZoom", this.onDrawerEventBound as EventListener);

        this.onInfoPaneEventBound = (event: Event) => this.onInfoPaneEvent(event as CustomEvent);
        InfoPane.instance.element.addEventListener("input", this.onInfoPaneEventBound as EventListener);
        InfoPane.instance.actionList.addEventListener("actionRemove", this.onInfoPaneEventBound as EventListener);

        this.onSelectedTrackBound = (event: Event) => this.onSelectedTrack(event as CustomEvent);
        MyTimeline.instance.addEventListener("selectedTrack", this.onSelectedTrackBound as EventListener);

        this.onStagePaneEventBound = (event: Event) => this.onStagePaneEvent(event as CustomEvent);
        StagePane.instance.addEventListener(StagePaneEvent.Resize, this.onStagePaneEventBound as EventListener);
    }

    override addClip(myClip: MyClip) {
        // add to our renderer
        this.content.prepend(myClip.content);

        // add to the parent
        super.addClip(myClip as Clip);

        // sort
        this.sortClips();

        return myClip;
    }

    override removeClip(myClip: MyClip) {
        // remove our MyClip
        myClip.destroy();
        if (myClip === this.selectedClip) {
            this.selectedClip = null;
        }

        // tell the parent
        super.removeClip(myClip as Clip);

        // sort the Clips
        this.sortClips();

        // reset the Actions
        this.actions.filter(action => action.clipId === myClip.id).forEach((action: any) => {
            action.clipId = 0;
            action.editorRef.setSymbol();
        });

        return this;
    }

    override addAction(action: unknown) {
        const myAction: MyAction = action as MyAction;

        // append the action's content to this content
        this.content.appendChild(myAction.content);

        // add to Track
        const trackAction = super.addAction(myAction.origAction);
    
        this.sortActions();
        this.adjustActions();

        return trackAction;
    }

    override removeAction(action: unknown) {
        if (action instanceof MyAction) {
            const myAction: MyAction = action as MyAction;

            // remove it
            myAction.destroy();

            // remove event from clip
            super.removeAction(myAction.origAction);
        }
        else {
            super.removeAction(action as any);
        }

        this.adjustActions();

        return this;
    }

    onDrawerEvent(event: CustomEvent) {
        if (MyTimeline.instance.selectedTrack !== this) {
            return;
        }

        switch (event.type) {
            case "updateTime": {
                // get the time
                const time = TimelinePane.instance.time;

                // assume nothing was selected
                this.selectedClip = this.clips.find(clip => time >= clip.startTime && time <= clip.endTime) as MyClip || null;

                // make sure they don't overlap
                this.checkClips();

                break;
            }
            case "updateZoom": {
                this.render();
                break;
            }
        }
    }

    onInfoPaneEvent(event: CustomEvent) {
        if (MyTimeline.instance.selectedTrack !== this) {
            return;
        }

        switch (event.type) {
            case "input": case "actionRemove": {
                this.adjustActions();
                break;
            }
        }
    }

    onSelectedTrack(event: CustomEvent) {
        this.onDrawerEvent(event);
    }

    onStagePaneEvent(event: CustomEvent) {
        this.render();
    }

    // called by `InfoPane`
    getActions() {
        const myActions: Array<MyAction> = [];
        this.actions.forEach((action: any) => {
            myActions.push(action.editorRef);
        });
        return myActions;
    }

    // called by `InfoPane`
    getActionsByTime(time: number) {
        const myActions: Array<MyAction> = [];
        this.actions.forEach((action: any) => {
            if (action.startTime === time) {
                myActions.push(action.editorRef);
            }
        });
        return myActions;
    }

    // called via `SortList`
    sortActionsById(array: Array<number>) {
        // sort based on the array passed
        this.actions.sort((a: any, b: any) => {
            let index = a.startTime - b.startTime; // time first
            return index === 0 ? array.indexOf(a.id) - array.indexOf(b.id) : index; // then sort by real order from passed array
        });
        
        // sort them visually
        this.adjustActions();
    }

    // sorts the Actions based on time
    sortActions() {
        this.actions.sort((a: any, b: any) => a.startTime - b.startTime);
    }

    // sorts the Action's visually
    adjustActions() {
        // set's the position along the Y
        this.actions.forEach((action: any) => {
            const otherActions = this.getActionsByTime(action.startTime);
            action.editorRef.indexInfo(otherActions.indexOf(action.editorRef), otherActions.length);
        });

        this.adjustActionsDependencies();
    }
    
    adjustActionsDependencies() {
        // draw's the connection between themselves, if any
        this.actions.forEach((action: any) => {
            action.editorRef.updateDependencies();
        });
    }

    sortClips() {
        this.clips.sort((a, b) => a.startTime - b.startTime);

        this.dispatchEvent(new CustomEvent(TimelineEvent.ClipsSorted, {
            bubbles: true,
            detail: {
                track: this
            }
        }));

        return this;
    }

    checkClips() {
        let anyErrors = false;

        this.clips.forEach((clip1: any) => {
            // assume there is no error
            clip1.error(false);

            this.clips.forEach((clip2: any) => {
                // don't do the same
                if (clip1 === clip2) {
                    return;
                }

                // mark if the clip is bad
                if (!(clip1.endTime < clip2.startTime || clip1.startTime > clip2.endTime)) {
                    anyErrors = true;
                    clip1.error(true);
                }
            });
        });

        return anyErrors;
    }

    addBookmark(name: string, time: number) {
        const bookmark = super.addBookmark(name, time) as any;
        
        // init 
        const bm = new Bookmark(bookmark);
        this.bookmarkRefs.push(bm);
        this.content.appendChild(bm.content);

        this.sortBookmarks();

        return bookmark;
    }

    removeBookmark(bookmark: any) {
        this.bookmarkRefs.find(ref => ref.bookmarkRef === bookmark)?.destroy();
        return super.removeBookmark(bookmark);
    }

    sortBookmarks() {
        this.bookmarks.sort((a: any, b: any) => a.time - b.time);
    }

    destroy() {
        // bookmarks
        this.bookmarkRefs.forEach(bm => bm.destroy());

        // remove the actions
        this.actions.forEach((action: any) => action.editorRef.destroy());

        // remove the clips
        this.clips.forEach(clip => (clip as MyClip).destroy());

        // remove our events
        TimelinePane.instance.drawer.removeEventListener("updateTime", this.onDrawerEventBound as EventListener);
        TimelinePane.instance.drawer.removeEventListener("updateZoom", this.onDrawerEventBound as EventListener);

        InfoPane.instance.element.removeEventListener("input", this.onInfoPaneEventBound as EventListener);
        InfoPane.instance.actionList.removeEventListener("actionRemove", this.onInfoPaneEventBound as EventListener);

        StagePane.instance.removeEventListener(StagePaneEvent.Resize, this.onStagePaneEventBound as EventListener);
    }

    toggle(show = false) {
        this.currentClip = null;
        this.clips.forEach(clip => (clip as MyClip).selected(false));
        this.content.style.display = show ? "block" : "none";

        if (show) {
            this.render();
            this.adjustActionsDependencies();
        }
        
        return this;
    }

    render() {
        this.bookmarkRefs.forEach(ref => ref.render());
        this.actions.forEach((action: any) => action.editorRef.render());
        this.clips.forEach(clip => (clip as MyClip).render());
        this.adjustActions();
        return this;
    }

    set selectedClip(value: MyClip | null) {
        // deselect
        this.clips.forEach(clip => (clip as MyClip).selected(false));

        // show the selected
        this._selectedClip = value?.selected(true) || null;

        // let everyone know
        this.dispatchEvent(new CustomEvent("selectedClip", {
            bubbles: true,
            detail: {
                clip: value
            }
        }));
    }
    get selectedClip() {
        return this._selectedClip;
    }
}