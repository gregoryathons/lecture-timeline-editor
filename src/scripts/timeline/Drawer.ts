import { TimelineEvent } from "lecture-timeline";
import MyTimeline from "./MyTimeline";
import { clamp, debounce, secondsToString, svgTransform } from "../utils";
import TimelinePane from "./TimelinePane";
import StateManager from "../StateManager";
import StagePane, { StagePaneEvent } from "../stage/StagePane";
import App, { AppEvent } from "../App";

export default class Drawer extends EventTarget {
    static instance: Drawer;

    isDraggingSeeker = false;
    isDraggingSelection = false;
    mousePt: DOMPoint;
    offsetLeft: number;

    private _zoom = 1;
    zoomMax = 4;
    zoomMin = 0.3;
    get zoom() {
        return this._zoom;
    }
    set zoom(value) {
        this._zoom = clamp(value, this.zoomMin, this.zoomMax);

        Array.from(this.elements.times.children).forEach((kid, index) => {
            const x = (index + 1) * this.oneSecondZoom;
            kid.setAttributeNS(null, "x", `${x}`);
            (kid as SVGTSpanElement).style.opacity = (this.zoom < 0.5 && !(index % 2) ? 0 : 1).toString();
        });
        Array.from(this.elements.lines.children).forEach((kid, index) => {
            const x = index * this.oneSecondZoom;
            kid.setAttributeNS(null, "x1", `${x}`);
            kid.setAttributeNS(null, "x2", `${x}`);
        });
        // Array.from(this.elements.dots.children).forEach((kid, index) => {
        //     const x = ((index + 1) * this.oneSecondZoom) * 0.1;
        //     kid.setAttributeNS(null, "cx", `${x}`);
        // });
        this.elements.lines.style.opacity = this.zoom.toString();

        this.adjustBounds();

        this.dispatchEvent(new CustomEvent("updateZoom"));
    };

    // the amount of pixels between a second
    oneSecondLength = 100;
    get oneSecondZoom() {
        return (this.oneSecondLength * this.zoom).roundToTenth();
    }

    // the time
    set time(value: number) {
        if (this.time !== value) {
            svgTransform(this.elements.gSeeker, (value * this.oneSecondZoom) + this.offsetLeft, 20);
            this.elements.seekerTime.querySelector("text")!.innerHTML = secondsToString(value);

            // if we aren't playing, don't dispatch the event
            if (!TimelinePane.instance.isPlaying) {
                this.dispatchEvent(new CustomEvent("updateTime"));
            }
        }
    }
    get time() {
        const x = svgTransform(this.elements.gSeeker)!.e - this.offsetLeft;
        return (x / this.oneSecondZoom).roundToTenth();
    }

    element: HTMLElement;
    elements: {[element: string]: SVGElement} = {};

    constructor(element: HTMLElement) {
        super();

        Drawer.instance = this;

        this.element = element;

        this.elements.svg = this.element.querySelector("svg") as SVGElement;

        this.elements.gTimelineWrapper = this.elements.svg.querySelector("g#timeline-wrapper") as SVGElement;
        this.elements.lines = this.elements.gTimelineWrapper.querySelector("#lines") as SVGElement;
        this.elements.selection = this.elements.gTimelineWrapper.querySelector("#selection") as SVGElement;
        this.elements.dots = this.elements.gTimelineWrapper.querySelector("#dots") as SVGElement;
        this.elements.times = this.elements.gTimelineWrapper.querySelector("#times") as SVGElement;
        this.elements.trackWrapper = this.elements.gTimelineWrapper.querySelector("#track-wrapper") as SVGElement;

        this.elements.gEmpty = this.elements.svg.querySelector("g#empty") as SVGElement;

        this.elements.gSeeker = this.elements.svg.querySelector("g#seeker") as SVGElement;
        this.elements.seekerTime = this.elements.gSeeker.querySelector(".seeker-time") as SVGElement;

        this.offsetLeft = svgTransform(this.elements.gTimelineWrapper)!.e;

        this.setupSVG();
    }

    setupEvents() {
        this.element.addEventListener("mousedown", (event: MouseEvent) => this.onInteraction(event));
        this.element.addEventListener("mousemove", (event: MouseEvent) => this.onInteraction(event));
        window.addEventListener("mouseup", (event: MouseEvent) => this.onInteraction(event));
        this.element.addEventListener("wheel", (event: WheelEvent) => this.onWheel(event));
        this.element.addEventListener("scroll", (event: Event) => this.onScroll(event));

        MyTimeline.instance.addEventListener("selectedTrack", () => window.requestAnimationFrame(() => this.adjustBounds()));
        MyTimeline.instance.addEventListener(TimelineEvent.ClipAdded, () => window.requestAnimationFrame(() => this.adjustBounds()));
        MyTimeline.instance.addEventListener(TimelineEvent.ActionAdded, () => window.requestAnimationFrame(() => this.adjustBounds()));
        MyTimeline.instance.addEventListener(TimelineEvent.TrackAdded, (event) => this.onTrackAdded(event as CustomEvent));
        MyTimeline.instance.addEventListener(TimelineEvent.TrackRemoved, (event) => this.onTrackRemoved(event as CustomEvent));

        StagePane.instance.addEventListener(StagePaneEvent.Resize, () => this.onResize());

        App.instance.addEventListener(AppEvent.Start, () => this.onResize());
        
        window.addEventListener("resize", () => this.onResize());
        window.addEventListener("message", (event: MessageEvent) => {
            switch (event.data.type) {
                // update locally to what the iFrame is doing
                case TimelineEvent.Update: {
                    this.onTimelineUpdate({ detail: event.data.data } as any);
                    break;
                }
            }
        });
    }

    setupSVG() {
        const length = 60 * 30; // 30 minutes

        // the bg lines
        for (let i = 0; i <= length; i++) {
            this.elements.lines.insertAdjacentHTML("beforeend", `<line x1="${i * this.oneSecondLength}" y1="${!i ? 0 : 20}" x2="${i * this.oneSecondLength}" y2="2000" />`);
        }

        // add the times
        for (let i = 0; i < length; i++) {
            this.elements.times.insertAdjacentHTML("beforeend", `<tspan x="${(i + 1) * this.oneSecondLength}">${i + 1}</tspan>`);
        }

        // // add the dots
        // for (let i = 0; i < length * 10; i++) {
        //     const dot: SVGCircleElement = document.createElementNS("http://www.w3.org/2000/svg", "circle");
        //     dot.setAttributeNS(null, "cx", "0");
        //     dot.setAttributeNS(null, "cy", "19");
        //     dot.setAttributeNS(null, "r", "1");
        //     this.elements.dots.appendChild(dot);
        // }
    }

    onResize() {
        const width = this.element.offsetWidth;
        const height = TimelinePane.instance.element.offsetHeight - TimelinePane.instance.toolbar.element.offsetHeight;
        
        // this.elements.svg.setAttributeNS(null, "width", width.toString());
        this.elements.svg.setAttributeNS(null, "height", (height - 12).toString());
        this.elements.svg.setAttributeNS(null, "viewBox", `0 0 ${width} ${height - 12}`);

        svgTransform(this.elements.seekerTime, 0, height - 62);

        this.adjustBounds();
    }

    onInteraction(event: MouseEvent) {
        // can't interact while playing
        if (TimelinePane.instance.isPlaying) {
            return;
        }
        if (event.button) {
            return;
        }

        switch (event.type) {
            case "mousedown": {
                if (event.target !== this.elements.svg) {
                    return;
                }

                // so the user clicks in the text time area
                if (event.offsetY <= 20) {
                    event.preventDefault();

                    this.isDraggingSeeker = true;
                    this.elements.seekerTime.style.display = "block";

                    this.time = this.getTime(event.offsetX);
                }
                // user is wanting to drag and select multiple Actions
                else {
                    event.preventDefault();

                    this.isDraggingSelection = true;
                    this.elements.selection.style.display = "block";
                    this.mousePt = new DOMPoint(event.offsetX, event.offsetY);
                    this.updateSelectionRect({ x: this.mousePt.x, y: this.mousePt.y, width: 1, height: 1 } as DOMRect, event.shiftKey);
                }
                break;
            }
            case "mousemove": {
                if (this.isDraggingSeeker) {
                    // keep scrolling
                    this.checkScroll(event.offsetX);

                    // set the time
                    this.time = this.getTime(event.offsetX);
                }
                else if (this.isDraggingSelection) {
                    this.updateSelectionRect({ 
                        x: Math.min(this.mousePt.x, event.offsetX),
                        y: Math.min(this.mousePt.y, event.offsetY),
                        width: Math.abs(event.offsetX - this.mousePt.x),
                        height: Math.abs(event.offsetY - this.mousePt.y)
                    } as DOMRect, event.shiftKey);
                }
                break;
            }
            case "mouseup": {
                if (this.isDraggingSeeker) {
                    this.isDraggingSeeker = false;
                    this.elements.seekerTime.style.display = "none";

                    this.time = this.getTime(event.offsetX);

                    // record the state
                    StateManager.instance.record();
                }

                if (this.isDraggingSelection) {
                    this.isDraggingSelection = false;
                    this.elements.selection.style.display = "none";
                }

                break;
            }
        }
    }

    checkScroll(x: number) {
        if (x - (this.element.scrollLeft + this.offsetLeft) < 100) {
            this.element.scrollLeft -= this.oneSecondZoom * 0.1;
        }
        else if (x > this.element.scrollLeft + this.element.offsetWidth - 100) {
            this.element.scrollLeft += this.oneSecondZoom * 0.1;
        }
    }

    // update the selection rect
    updateSelectionRect(rect: DOMRect, shiftKey: boolean) {
        const selection = this.elements.selection;
        selection.setAttribute("x", `${rect.x - this.offsetLeft}`);
        selection.setAttribute("y", `${rect.y}`);
        selection.setAttribute("width", `${rect.width}`);
        selection.setAttribute("height", `${rect.height}`);
        
        this.dispatchEvent(new CustomEvent("updateSelection", { 
            detail: { 
                rect: rect, 
                shiftKey: shiftKey 
            } 
        }));
    }

    onWheel(event: WheelEvent | null = null) {
        // can't interact
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        if (event) {
            event.preventDefault();

            // scroll pan
            if (Math.abs(event.deltaX) > 1) {
                this.element.scrollLeft += event.deltaX;
            }
            // scroll zoom enough
            if (Math.abs(event.deltaY) < 1) {
                return;
            }

            // increment/decrement
            this.zoom = (this.zoom + (event.deltaY * 0.001)).roundToTenth();
        }

        // dispatch event
        this.dispatchEvent(new CustomEvent("updateTime"));
    }

    onScroll(event: Event) {
        const left = this.element.scrollLeft;
        
        // move the empty over 
        svgTransform(this.elements.gEmpty, left, 0);
        
        // Array.from(this.elements.dots.children).forEach((kid, index) => {
        //     const x = ((index + 1) * this.oneSecondZoom) * 0.1;
        //     kid.setAttributeNS(null, "cx", `${x}`);
        // });
    }

    onTimelineUpdate(event: CustomEvent | number) {
        this.time = typeof event === "number" ? event : event.detail.time;

        let time = this.time * this.oneSecondZoom;

        // scroll when playing
        if (this.element.scrollLeft + time > this.element.scrollLeft + this.element.offsetWidth * 0.5) {
            this.element.scrollLeft = time - this.element.offsetWidth * 0.5;
        }
        else if (time < this.element.scrollLeft) {
            this.element.scrollLeft = time - this.element.offsetWidth * 0.5;
        }
    }

    onTrackAdded(event: CustomEvent) {
        this.elements.trackWrapper.appendChild(event.detail.track.content);
    }

    onTrackRemoved(event: CustomEvent) {
        // this.elements.trackWrapper.removeChild(event.detail.track.content);
    }

    getTime(x: number) {
        // get the assumed seeker position
        const n = 10 * this.zoom;
        let time = Math.round((x - this.offsetLeft) / n) * n; // so a tenth of a second is selected
        time = clamp(time, 0, Infinity); // TODO - probably should be MyTimeline.instance.selectedTrack.duration?
        return (time / this.oneSecondZoom).roundToTenth();
    }

    adjustBounds() {
        const selectedTrack = MyTimeline.instance.selectedTrack;
        let width = this.element.offsetWidth;
        let length = width;

        if (selectedTrack) {
            // get the new total duration and set the width of the root SVG
            length = (selectedTrack.duration + 30) * this.oneSecondZoom;
            if (length < width) {
                length = width;
            }
        }

        this.elements.svg.setAttributeNS(null, "width", length.toString());
    }
}