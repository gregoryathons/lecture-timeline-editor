import App from "../App";
import { TimelineEvent } from "lecture-timeline";
import StagePane from "../stage/StagePane";
import { secondsToString } from "../utils";
import MyTimeline from "./MyTimeline";
import MyTrack from "./MyTrack";
import TimelinePane from "./TimelinePane";

export default class Toolbar extends EventTarget {
    static instance: Toolbar;

    isPlaying: boolean;

    element: HTMLElement;
    elements: {[element: string]: HTMLElement} = {};

    previewWindow: Window;

    constructor(element: HTMLElement) {
        super();

        Toolbar.instance = this;

        this.element = element;

        // our columns in the Toolbar
        const columns = this.element.querySelectorAll(".columns");
        this.elements.leftColumn = columns[0] as HTMLElement;
        this.elements.midColumn = columns[1] as HTMLElement;
        this.elements.rightColumn = columns[2] as HTMLElement;
    }

    setupEvents() {
        this.element.addEventListener("click", (event: Event) => this.onClick(event));
        this.element.addEventListener("input", (event: Event) => this.onInput(event));

        const inputZoom = this.elements.rightColumn.querySelector(".zoom input") as HTMLInputElement;
        inputZoom.min = TimelinePane.instance.drawer.zoomMin.toString();
        inputZoom.max = TimelinePane.instance.drawer.zoomMax.toString();

        TimelinePane.instance.drawer.addEventListener("updateTime", () => window.requestAnimationFrame(() => this.onUpdateTime()));
        TimelinePane.instance.drawer.addEventListener("updateZoom", () => window.requestAnimationFrame(() => this.onUpdateZoom()));

        MyTimeline.instance.addEventListener(TimelineEvent.TrackAdded, (event) => this.onTrackAdded(event as CustomEvent));
        MyTimeline.instance.addEventListener(TimelineEvent.TrackRemoved, (event) => this.onTrackRemoved(event as CustomEvent));
        MyTimeline.instance.addEventListener("selectedTrack", (event) => this.selectedTrack && this.onTrackSelected(this.selectedTrack.id));

        // listen for postMesssages
        window.addEventListener("message", (event: MessageEvent) => {
            const data = event.data;

            switch (data.type) {
                // when the iframe tells us it's playing
                case TimelineEvent.Play: {
                    this.onPlay(data.data);
                    break;
                }
                // when the iframe tells us it's paused
                case TimelineEvent.Pause: {
                    this.onPause();
                    break;
                }
                // action!
                case TimelineEvent.ActionTriggered: {
                    this.onActionTriggered(data.data);
                    break;
                }
                case "preview-window-loaded": {
                    this.syncPreviewWindow();
                    break;
                }
                case "preview-window-import-complete": {
                    StagePane.instance.post({
                        type: TimelineEvent.Play,
                        time: this.time
                    }, this.previewWindow);
                    break;
                }
            }
        });
    }

    onPlay(data: any) {
        this.isPlaying = true;

        const btn = this.elements.midColumn.querySelector("button") as HTMLButtonElement;
        btn.querySelector("span")!.innerText = "pause";
        btn.classList.remove("play");
        btn.classList.add("pause");

        // show the Track selected in the select drop down
        this.onTrackSelected(data.trackId);

        this.elements.leftColumn.querySelector(`select[name="tracks"]`)?.dispatchEvent(new CustomEvent("input", { bubbles: true }));
    }

    onPause() {
        this.isPlaying = false;

        const btn = this.elements.midColumn.querySelector("button") as HTMLButtonElement;
        btn.querySelector("span")!.innerText = "play_arrow";
        btn.classList.remove("pause");
        btn.classList.add("play");
    }

    async onClick(event: Event) {
        const target = event.target as HTMLElement;

        // play/pause the Timeline
        if (target.classList.contains("play") || target.classList.contains("pause")) {
            if (MyTimeline.instance.selectedTrack?.checkClips()) {
                alert("Some audio Clips overlap!");
                return;
            }
    
            // play if not playing, or pause
            if (!this.isPlaying) {
                // open in a new window
                if ((event as KeyboardEvent).shiftKey) {
                    const path = (StagePane.instance.elements.iframe as HTMLIFrameElement).src;
                    this.previewWindow = window.open(path + `?${Date.now()}`, "lecture-timeline")!;
                }
                // same window
                else {
                    await StagePane.instance.updateDisplay(true, false);
                    StagePane.instance.play();
                }
            }
            else {
                StagePane.instance.pause();
            }
        }
        // add/remove/edit Track
        else if (target.classList.contains("add-track")) {
            // add the Track
            MyTimeline.instance.addTrack(new MyTrack({ name: `TEMP ${MyTimeline.instance.tracks.length}` }));

            this.hideTrackMenu();
        }
        else if (target.classList.contains("edit-track")) {
            if (this.selectedTrack && !this.selectedTrack.isMain) {
                const newName = prompt(`Rename Track "${this.selectedTrack.name}" to be:`, this.selectedTrack.name);
                // we have the new name
                if (newName) {
                    this.selectedTrack.name = newName;
                    this.updateTrackNames();
                }

                this.hideTrackMenu();
            }
        }
        else if (target.classList.contains("remove-track")) {
            if (this.selectedTrack && !this.selectedTrack.isMain && confirm(`Remove the Track "${this.selectedTrack.name}"?`)) {
                // remove the Track
                MyTimeline.instance.removeTrack(this.selectedTrack);

                this.hideTrackMenu();
            }
        }
        // forward/backward
        else if (target.classList.contains("time-offset-backward") || target.classList.contains("time-offset-forward")) {
            const element = <HTMLInputElement> this.element.querySelector(`input[name="time-offset-range"]`);
            // update selectedTrack property
            if (this.selectedTrack) {
                this.selectedTrack.timeOffset += target.classList.contains("time-offset-backward") ? -0.1 : 0.1;
                this.selectedTrack.timeOffset = this.selectedTrack.timeOffset.roundToTenth();

                // update the values and output
                element.value = this.selectedTrack.timeOffset.toString();
                (this.elements.leftColumn.querySelector(`.time-offset span.output`) as HTMLElement).innerText = secondsToString(this.selectedTrack.timeOffset);
            }
            // dispatch
            this.dispatchEvent(new CustomEvent("input"));
        }
    }

    onInput(event: Event) {
        const target = event.target as HTMLElement;

        // admin selected a new track
        if ((target as HTMLSelectElement).name === "tracks") {
            const select = target as HTMLSelectElement;
            const trackId = parseInt(select.options[select.selectedIndex].value);
            const myTrack = MyTimeline.instance.getTrack(trackId) as MyTrack;

            if (myTrack) {
                MyTimeline.instance.selectedTrack = myTrack;
            }
        }
        // admin is changing the time offset range
        else if ((target as HTMLInputElement).name === "time-offset-range") {
            const value = parseFloat((target as HTMLInputElement).value);

            // update the output
            const inputText = (target as HTMLElement).parentElement!.querySelector(`span.output`)! as HTMLElement;
            inputText.innerText = secondsToString(value);

            // update property
            if (this.selectedTrack) {
                this.selectedTrack.timeOffset = value;
            }

            this.dispatchEvent(new CustomEvent("input"));
        }
        // update the zoom
        else if ((target as HTMLInputElement).name === "zoom") {
            TimelinePane.instance.drawer.zoom = parseFloat((target as HTMLInputElement).value);
            TimelinePane.instance.drawer.dispatchEvent(new CustomEvent("updateTime"));
        }
    }

    // hide it
    hideTrackMenu() {
        const menuContent = this.elements.leftColumn.querySelector(".item-menu__content") as HTMLElement;
        menuContent.style.display = "none";
        // silly I know
        setTimeout(() => menuContent.style.display = "", 1);
    }

    onUpdateTime() {
        const spanTime = this.elements.rightColumn.querySelector(".time span.time-output") as HTMLSpanElement;
        spanTime.innerText = secondsToString(this.time);
    }

    onUpdateZoom() {
        const inputZoom = this.elements.rightColumn.querySelector(".zoom input") as HTMLInputElement;
        inputZoom.value = TimelinePane.instance.zoom.toString();
    }

    onTrackAdded(event: CustomEvent) {
        const myTrack = event.detail.track;
        
        const option = document.createElement("option");
        option.value = myTrack.id.toString();
        option.selected = true;

        const select = this.elements.leftColumn.querySelector(`select[name="tracks"]`) as HTMLSelectElement;
        select.options.add(option);

        this.updateTrackNames();

        select.dispatchEvent(new CustomEvent("input", { bubbles: true }));
    }

    onTrackRemoved(event: CustomEvent) {
        const myTrack = event.detail.track;

        const select = this.elements.leftColumn.querySelector(`select[name="tracks"]`) as HTMLSelectElement;
        const option = select.querySelector(`option[value="${myTrack.id}"]`) as HTMLOptionElement;
        select.removeChild(option);

        if (select.options.length) {
            (select.options[0] as HTMLOptionElement).selected = true;
            select.dispatchEvent(new CustomEvent("input", { bubbles: true }));
        }

        this.updateTrackNames();
    }

    // selects the option, but does not dispatchEvent on the select - because maximum call stack error would occur
    onTrackSelected(id: number) {
        const myTrack = MyTimeline.instance.getTrack(id);

        const select = this.elements.leftColumn.querySelector(`select[name="tracks"]`) as HTMLSelectElement;
        const option = select.querySelector(`option[value="${id}"]`) as HTMLOptionElement;
        if (option) {
            option.selected = true;
        }

        // show the time offset
        const div = this.elements.leftColumn.querySelector(`div.time-offset`) as HTMLElement;
        div.style.display = myTrack?.isMain ? "none" : "";

        // set this Track's time offset value
        if (myTrack && !myTrack.isMain) {
            const inputRange = div.querySelector(`input[type="range"]`) as HTMLInputElement;
            inputRange.min = "0";
            inputRange.max = (MyTimeline.instance.getTrack(1)!.duration || 10).toString();
            inputRange.value = myTrack.timeOffset.toString();
            inputRange.dispatchEvent(new CustomEvent("input", { bubbles: true }));
        }
    }

    onActionTriggered(data: any) {
        if (data.type === "Play") {
            this.onPlay(data);
        }
    }

    updateTrackNames() {
        const select = this.elements.leftColumn.querySelector(`select[name="tracks"]`) as HTMLSelectElement;
        
        const tracks = MyTimeline.instance.tracks;
        tracks.forEach((track, i) => {
            select.options[i].text = `${track.name} (${(i + 1)} of ${tracks.length})`;
        });
    }

    syncPreviewWindow() {
        StagePane.instance.post({
            type: "setLoaderRootPath",
            rootPath: MyTimeline.instance.loader.rootPath
        }, this.previewWindow);

        // prevents objects from being passed
        const soundResources = JSON.parse(JSON.stringify(MyTimeline.instance.loader.resources.sounds));

        StagePane.instance.post({
            type: "addResource",
            name: "sounds",
            resources: soundResources
        }, this.previewWindow);

        // prevents objects from being passed
        const imageResources = JSON.parse(JSON.stringify(MyTimeline.instance.loader.resources.images));

        StagePane.instance.post({
            type: "addResource",
            name: "images",
            resources: imageResources
        }, this.previewWindow);

        // sync our local with the `previewWindow`
        StagePane.instance.syncDisplay(this.previewWindow);
    }

    get selectedTrack() {
        return <MyTrack> MyTimeline.instance.selectedTrack;
    }
    get time() {
        return TimelinePane.instance.time;
    }
}