import MyTimeline from "./MyTimeline";
import MyTrack from "./MyTrack";
import MyClip from "./MyClip";
import Drawer from "./Drawer";
import Toolbar from "./Toolbar";

export default class TimelinePane extends EventTarget {
    static instance: TimelinePane;

    element: HTMLElement;
    elements: {[element: string]: HTMLElement} = {};

    toolbar: Toolbar;
    drawer: Drawer;

    constructor() {
        super();

        TimelinePane.instance = this;

        this.element = document.querySelector("#timeline.pane") as HTMLElement;
        
        // our Toolbar, Drawer
        this.toolbar = new Toolbar(this.element.querySelector("#timeline-toolbar") as HTMLElement);
        this.drawer = new Drawer(this.element.querySelector("#timeline-drawer-wrapper") as HTMLElement);
    }

    setupEvents() {
        this.toolbar.setupEvents();
        this.drawer.setupEvents();
        
        this.element.addEventListener("click", (event) => this.onClick(event));
    }

    onClick(event: Event) {
    }

    get isPlaying() {
        return this.toolbar.isPlaying;
    }
    get zoom() {
        return this.drawer.zoom;
    }
    get time() {
        return this.drawer.time;
    }
    set time(value: number) {
        this.drawer.time = value;
    }
    get oneSecondLength() {
        return this.drawer.oneSecondLength;
    }
    get oneSecondZoom() {
        return this.drawer.oneSecondZoom;
    }
    get width() {
        return this.drawer.element.scrollWidth;
    }
    get selectedTrack() {
        return <MyTrack> MyTimeline.instance.selectedTrack;
    }
    get selectedClip() {
        return <MyClip> this.selectedTrack?.selectedClip;
    }
}