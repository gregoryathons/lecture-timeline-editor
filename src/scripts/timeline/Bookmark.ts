import { clamp, svgTransform } from "../utils";
import MyTimeline from "./MyTimeline";
import TimelinePane from "./TimelinePane";

interface IDragInfo {
    temp: boolean;
    drag: boolean;
    x: number;
}

export default class Bookmark {
    bookmarkRef: any; // iBookmark from Track
    content: SVGGElement;

    onDocumentInteractionBound: Function;
    dragInfo: IDragInfo = <IDragInfo> {};

    constructor(bookmarkRef: any) {
        this.bookmarkRef = bookmarkRef;

        this.content = document.createElementNS("http://www.w3.org/2000/svg", "g");
        this.content.classList.add("bookmark");
        this.content.setAttributeNS(null, "transform", "translate(0, 0)");

        this.content.insertAdjacentHTML("beforeend", `<polyline points="-6,-8 6,-8 6,2 0,8 -6,2" />`);
        this.content.insertAdjacentHTML("beforeend", `<line x1="0" y1="8" x2="0" y2="2000" />`);
        this.content.insertAdjacentHTML("beforeend", `<text alignment-baseline="middle">${bookmarkRef.name}</text>`);

        this.onDocumentInteractionBound = (event: MouseEvent) => this.onDocumentInteraction(event);
        document.addEventListener("mousedown", this.onDocumentInteractionBound as EventListener);

        this.render();
    }

    onDocumentInteraction(event: MouseEvent) {
        switch (event.type) {
            case "mousedown": {
                if (event.target === this.content.querySelector("polyline")) {
                    event.preventDefault();

                    this.dragInfo.temp = true;
                    this.dragInfo.x = event.clientX;

                    document.addEventListener("mousemove", this.onDocumentInteractionBound as EventListener);
                    document.addEventListener("mouseup", this.onDocumentInteractionBound as EventListener);
                }

                break;
            }
            case "mousemove": {
                if (this.dragInfo.temp) {
                    if (Math.abs(event.clientX - this.dragInfo.x) >= 8) {
                        this.dragInfo.temp = false;
                        this.dragInfo.drag = true;
                    }
                }

                if (this.dragInfo.drag) {
                    // make sure it scrolls with mouse
                    TimelinePane.instance.drawer.checkScroll(event.offsetX);

                    // set the time
                    this.bookmarkRef.time = TimelinePane.instance.drawer.getTime(event.offsetX);

                    // sort them
                    this.selectedTrack?.sortBookmarks();

                    this.render();
                }

                break;
            }
            case "mouseup": {
                this.dragInfo.temp = this.dragInfo.drag = false;

                document.removeEventListener("mousemove", this.onDocumentInteractionBound as EventListener);
                document.removeEventListener("mouseup", this.onDocumentInteractionBound as EventListener);
                break;
            }
        }
    }

    render() {
        svgTransform(this.content, this.bookmarkRef.time * TimelinePane.instance.oneSecondZoom, 10);
    }

    destroy() {
        this.content.remove();

        // remove event
        document.removeEventListener("mousedown", this.onDocumentInteractionBound as EventListener);
    }


    get selectedTrack() {
        return MyTimeline.instance.selectedTrack;
    }
};