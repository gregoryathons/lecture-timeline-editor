import InfoPane from "../info/InfoPane";
import { clamp, lerp, rand, svgTransform } from "../utils";
import MyClip from "./MyClip";
import MyTimeline from "./MyTimeline";
import MyTrack from "./MyTrack";
import TimelinePane from "./TimelinePane";

interface IDragInfo {
    temp: boolean;
    drag: boolean;
    x: number;
    startTime: number;
}

// finds the available space in the Timeline's viewer
export function findOpening(timeline: MyTimeline, rect: DOMRect) {
    Array.from(timeline.content.children).forEach(kid => {
        const css = window.getComputedStyle(kid);
        let x = parseInt(css.left);
        let y = parseInt(css.top);
        if (isNaN(x)) { x = 0; }
        if (isNaN(y)) { y = 0; }

        if (!rect.x) { rect.x = 0; }
        if (!rect.y) { rect.y = 0; }
        if (x === rect.x) { rect.x += 2; }
        if (y === rect.y) { rect.y += 2; }
    });
};

export default class MyAction {
    origAction: any;

    color: string;
    content: SVGGElement;
    onDocumentInteractionBound: Function;
    onInfoPaneInteractionBound: Function;
    onDrawerInteractionBound: Function;
    dragInfo: IDragInfo = <IDragInfo> { startTime: -1 };

    constructor(origAction: any) {
        this.origAction = origAction;
        this.origAction.editorRef = this;

        // show it visually, even if there is one there
        this.content = document.createElementNS("http://www.w3.org/2000/svg", "g");
        this.content.classList.add("action");
        this.content.setAttributeNS(null, "transform", "translate(0, 0)");

        this.content.insertAdjacentHTML("beforeend", `<rect class="bounds" x="-10" y="-10" height="20" />`);
        this.content.insertAdjacentHTML("beforeend", `<text x="12" y="6" />`);
        this.content.insertAdjacentHTML("beforeend", `<line class="duration" x1="0" y1="0" y2="0" />`);

        this.color = `${rand(50, 150)}, ${rand(50, 150)}, ${rand(50, 150)}`;
        this.setSymbol();

        // our events
        this.onDocumentInteractionBound = (event: MouseEvent) => this.onDocumentInteraction(event);
        document.addEventListener("mousedown", this.onDocumentInteractionBound as EventListener);
        document.addEventListener("mouseover", this.onDocumentInteractionBound as EventListener);
        document.addEventListener("mouseout", this.onDocumentInteractionBound as EventListener);
        document.addEventListener("dblclick", this.onDocumentInteractionBound as EventListener);

        this.onInfoPaneInteractionBound = (event: Event) => this.onInfoPaneInteraction(event);
        InfoPane.instance.element.addEventListener("input", this.onInfoPaneInteractionBound as EventListener);

        this.onDrawerInteractionBound = (event: Event) => this.onDrawerInteraction(event as CustomEvent);
        TimelinePane.instance.drawer.addEventListener("updateSelection", this.onDrawerInteractionBound as EventListener);

        // fire event
        this.onInfoPaneInteraction(new CustomEvent("input"));
    }

    setSymbol() {
        (this.content.querySelector(".symbol") as SVGElement)?.remove();

        if (!this.clipId) {
            this.content.insertAdjacentHTML("beforeend", `<rect class="symbol" transform="rotate(45)" x="-6" y="-6" width="12" height="12" />`);
        }
        else {
            this.content.insertAdjacentHTML("beforeend", `<circle class="symbol" r="6" />`);
        }

        // set the color
        this.setColor();
    }

    setColor(color: string = "") {
        this.color = color || this.color;

        this.content.querySelector(".duration")?.setAttributeNS(null, "stroke", `rgba(${this.color}, 0.3)`);
        this.content.querySelector(".symbol")?.setAttributeNS(null, "fill", `rgba(${this.color}, 1)`);

        Array.from(this.content.querySelectorAll("path.relation")).forEach(path => {
            (path as SVGPathElement).style.stroke = `rgba(${this.color}, 0.3)`;
        });
    }

    destroy() {
        // remove this id from ALL Actions that are dependent on it, same with Groups
        MyTimeline.instance.tracks.forEach(track => {
            track.actions.forEach((action: any) => {
                // not assigned to it anymore
                if (action.editorRef.isDependent) {
                    const index = action.otherIds.indexOf(this.id);
                    if (index >= 0) {
                        action.otherIds.splice(index, 1);
                    }
                }
            });

            track.groups.forEach((group: any, i: number, array: any) => {
                const index = group.otherIds.indexOf(this.id);
                if (index >= 0) {
                    group.otherIds.splice(index, 1);
                }
                // nothing left on this group? remove it
                if (!group.otherIds.length) {
                    track.removeGroup(group);
                }
            });
        });

        // remove from the drawer
        this.content.remove();

        // remove events
        document.removeEventListener("mousedown", this.onDocumentInteractionBound as EventListener);
        document.removeEventListener("mouseover", this.onDocumentInteractionBound as EventListener);
        document.removeEventListener("mouseout", this.onDocumentInteractionBound as EventListener);
        document.removeEventListener("dblclick", this.onDocumentInteractionBound as EventListener);

        // remove the listeners
        InfoPane.instance.element.removeEventListener("input", this.onInfoPaneInteractionBound as EventListener);
        TimelinePane.instance.drawer.addEventListener("updateSelection", this.onDrawerInteractionBound as EventListener);
    }

    render() {
        const zoom = TimelinePane.instance.oneSecondZoom;
        const size = this.isDependent && this.otherIds.length && this.otherIds[0] > 0 ? 1 / 0.7 : 1;//svgTransform(this.content)!.a;

        const width = this.content.querySelector("text")?.getBBox().width || 0;
        this.content.querySelector("rect.bounds")?.setAttributeNS(null, "width", (width + 24)?.toString());
        this.content.querySelector("line.duration")?.setAttributeNS(null, "x2", (this.origAction.duration * zoom * size).toString());

        // is enabled/disabled?
        this.content.classList.remove("disabled");
        if (!this.origAction.enabled) {
            this.content.classList.add("disabled");
        }

        // set the position
        svgTransform(this.content, this.startTime * zoom);
    }

    onDocumentInteraction(event: MouseEvent, otherAction: MyAction | null = null) {
        // can't interact when playing
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        const mySymbol = event.target === this.content.querySelector(".symbol");

        switch (event.type) {
            // if the user clicked our canvas
            case "mousedown": {
                if (!event.button && (mySymbol || otherAction)) {
                    this.dragInfo.temp = true;
                    this.dragInfo.x = event.offsetX;
                    this.dragInfo.startTime = otherAction ? otherAction.startTime - this.startTime : 0;

                    const wasSelected = this.content.classList.contains("selected");

                    if (event.shiftKey) {
                        if (wasSelected) {
                            this.content.classList.remove("selected");
                        }
                        else {
                            this.content.classList.add("selected");
                        }
                    }
                    // deselect everything
                    else if (!wasSelected) {
                        this.selectedTrack.actions.forEach((action: any) => {
                            action.editorRef.content.classList.remove("selected");
                        });
                    }
                    // pass on the event to the other Actions
                    else if (mySymbol) {
                        this.selectedTrack?.actions.forEach((action: any) => {
                            if (this !== action.editorRef && action.editorRef.content.classList.contains("selected") && !action.editorRef.dragInfo.temp) {
                                action.editorRef.onDocumentInteraction(event, this);
                            }
                        });
                    }

                    // apply additional events
                    if (mySymbol) {
                        document.addEventListener("mousemove", this.onDocumentInteractionBound as EventListener);
                        document.addEventListener("mouseup", this.onDocumentInteractionBound as EventListener);
                    }
                }
                break;
            }
            case "mousemove": {
                // did the user drag it far enough to be considered draggable?
                if (this.dragInfo.temp) {
                    if (Math.abs(event.offsetX - this.dragInfo.x) >= 8) {
                        this.dragInfo.temp = false;
                        this.dragInfo.drag = true;
                    }
                }

                // user is dragging
                if (this.dragInfo.drag) {
                    // make sure it scrolls
                    TimelinePane.instance.drawer.checkScroll(event.offsetX);

                    // show everything
                    this.highlightRelatedActions();

                    // update all the selected Actions
                    if (mySymbol) {
                        this.selectedTrack?.actions.forEach((action: any) => {
                            if (this !== action.editorRef && action.editorRef.content.classList.contains("selected")) {
                                action.editorRef.onDocumentInteraction(event, this);
                            }
                        });
                    }

                    let time = TimelinePane.instance.drawer.getTime(event.offsetX) - (otherAction ? this.dragInfo.startTime : 0);

                    if (this.clipId) {
                        const myClip = this.selectedTrack.getClip(this.clipId) as MyClip;
                        time = clamp(time, myClip.startTime, myClip.endTime);
                    }
                    else {
                        time = clamp(time, 0, Infinity);
                    }

                    this.startTime = time;

                    // if this Action is a dependent, make sure it doesn't come before
                    if (this.isDependent) {
                        this.superiors.forEach((action: any) => {
                            if (this.startTime < action.startTime && this.origAction.track === action.track) {
                                this.startTime = action.startTime;
                            }
                        });
                    }
                    // make sure the dependents don't start before this Action
                    else {
                        this.dependents.forEach((action: any) => {
                            if (action.startTime < this.startTime && action.track === this.origAction.track) {
                                action.editorRef.startTime = this.startTime;
                            }
                        });
                    }

                    // TODO - fine tune this?
                    this.selectedTrack.adjustActionsDependencies();
                }

                break;
            }
            case "mouseup": {
                let dispatch = false;

                if (this.dragInfo.drag) {
                    // sort depending on the time
                    this.selectedTrack.sortActions();
                    // tell the Track this Action is attached to sort the actions
                    this.selectedTrack.adjustActions();
                    // determines if this Action should dispatch any events
                    dispatch = true;
                }

                // (de)attach to clip
                if (!this.dragInfo.drag && event.altKey && mySymbol) {
                    const lastClipId = this.clipId;
                    const clip = this.selectedTrack.clips.find(clip => this.startTime >= clip.startTime && this.endTime <= clip.endTime);

                    // remove the Action from Clip
                    if (this.clipId) {
                        this.clipId = 0;
                    }
                    // set the Action to the Clip
                    else if (clip && this.startTime >= clip.startTime && this.endTime <= clip.endTime) {
                        this.clipId = clip.id;
                    }
                    // re-renders the symbol
                    this.setSymbol();

                    // determines if this Action should dispatch any events
                    dispatch = lastClipId !== this.clipId;
                }

                if (dispatch) {
                    // dispatch the event
                    TimelinePane.instance.dispatchEvent(new CustomEvent("actionUpdate", { detail: { action: this } }));
                    // so the ActionList updates
                    TimelinePane.instance.drawer.dispatchEvent(new CustomEvent("updateTime"));
                }

                this.dragInfo.temp = this.dragInfo.drag = false;

                // remove additional events
                document.removeEventListener("mousemove", this.onDocumentInteractionBound as EventListener);
                document.removeEventListener("mouseup", this.onDocumentInteractionBound as EventListener);

                break;
            }
            // hide all the Actions except related to highlighted
            case "mouseover": {
                if (mySymbol) {
                    if (!this.dragInfo.drag) {
                        this.highlightRelatedActions(this.id);
                    }
                }
                break;
            }
            // turn up the opacity on the Actions
            case "mouseout": {
                if (mySymbol) {
                    this.highlightRelatedActions();
                }
                break;
            }
            // double clicked on it
            case "dblclick": {
                if (mySymbol) {
                    // go to that point in time
                    TimelinePane.instance.time = this.startTime;

                    // mimic double click on the Action to expand
                    setTimeout(() => {
                        InfoPane.instance.actionList.showAction(this.id)?.scrollIntoView();
                    }, 500);
                }
                break;
            }
        }
    }

    onInfoPaneInteraction(event: Event) {
        switch (event.type) {
            case "input": {
                window.requestAnimationFrame(() => {
                    this.content.querySelector("text")!.innerHTML = this.origAction.editorName();
                    this.render();
                });
                break;
            }
        }
    }

    onDrawerInteraction(event: CustomEvent) {
        switch (event.type) {
            case "updateSelection": {
                const rect = event.detail.rect as DOMRect;
                const shiftKey = event.detail.shiftKey;
                const myBox = this.content.getCTM()!;
                const wasSelected = this.content.classList.contains("selected");

                // in bounds
                if (rect.x <= myBox.e && rect.x + rect.width >= myBox.e && rect.y <= myBox.f && rect.y + rect.height >= myBox.f) {
                    // mark if it should be selected of not
                    if (!shiftKey) {
                        this.content.classList.add("selected");
                    }
                    else if (wasSelected) {
                        this.content.classList.remove("selected");
                    }
                }
                else {
                    this.content.classList.remove("selected");
                }

                break;
            }
        }
    }

    highlightRelatedActions(id: number = 0) {
        const actions = MyTimeline.instance.selectedTrack?.actions;
        
        if (id) {
            let relatedIds: Array<number> = [id];

            // climb up to the root
            const findRoot = (action: any) => {
                if (!action) {
                    return 0;
                }
                (action.otherIds || []).forEach((otherId: number) => {
                    const id = findRoot(MyTimeline.instance.getAction(otherId));
                    if (id > 0 && relatedIds.indexOf(id) < 0) {
                        relatedIds.push(id);
                    }
                });
                return action.id;
            };
            findRoot(this.origAction);

            // crawl down through the children
            actions?.forEach((action: any) => {
                (action.otherIds || []).forEach((otherId: number) => {
                    if (relatedIds.indexOf(otherId) >= 0 && relatedIds.indexOf(action.id) < 0) {
                        relatedIds.push(action.id);
                    }
                });
            });

            // either hide or show the Actions related 
            actions?.forEach((action: any) => {
                const classList = action.editorRef.content.classList;
                const exists = relatedIds.indexOf(action.id) >= 0;
                if (exists) {
                    classList.remove("hide");
                }
                else {
                    classList.add("hide");
                }
            });
        }
        // revert back to full opacity
        else {
            actions?.forEach((action: any) => {
                action.editorRef.content.classList.remove("hide");
            });
        }
    }

    indexInfo(index: number, length: number) {
        const size = this.isDependent && this.otherIds.length && this.otherIds[0] > 0 ? 0.7 : 1;
        const top = 20 + 20;
        const bottom = TimelinePane.instance.drawer.element.offsetHeight - 20;
        let y = lerp(top, bottom, index / length);

        if (this.isDependent && this.isValid) {
            this.superiors.forEach((action: any) => {
                if (action.track === this.origAction.track) {
                    // console.log("other id", id, "this", this.origAction, `other (${id})`, otherAction);
                    const otherY = svgTransform(action.editorRef.content)!.f;
                    if (otherY >= y) {
                        y = otherY + 20;
                    }
                    // set color of this to the other we are dependent on
                    this.setColor(action.editorRef.color);
                }
            });
        }

        // console.log("setting", this.id);

        svgTransform(this.content, this.startTime * TimelinePane.instance.oneSecondZoom, y, size, size);
    }

    updateDependencies() {
        const myTransform = svgTransform(this.content)!;

        // remove old paths
        Array.from(this.content.querySelectorAll("path.relation")).forEach(path => path.remove());

        this.superiors.forEach((action: any) => {
            if (action.track === this.origAction.track) {
                const otherTransform = svgTransform(action.editorRef.content)!;
                const scale = 1 / 0.7;//otherTransform.a;
                let dx = otherTransform.e - myTransform.e;
                let dy = otherTransform.f - myTransform.f;

                let points = `M 0 0 C 0 ${-20 * scale}, ${dx * scale} ${(dy + 20) * scale}, ${dx * scale} ${dy * scale}`;

                this.content.insertAdjacentHTML("beforeend", `<path class="relation" d="${points}" />`);
            }
        });

        // set the color
        this.setColor();

        // remove error
        this.content.classList.remove("error");
        if (!this.isValid) {
            this.content.classList.add("error");
        }
    }

    // is dependent on another Action, it checks if the `otherIds` exists 
    get isDependent() {
        return Array.isArray(this.origAction.otherIds) && this.origAction.otherIds[0] > 0;
    }
    // return the Actions this dependent relies on
    get superiors() {
        const actions: any = [];
        this.otherIds.forEach(id => {
            const action = MyTimeline.instance.getAction(id) as any;
            if (action) {
                actions.push(action);
            }
        });
        return actions;
    }
    // get all the Actions that are dependent on this
    get dependents() {
        return this.selectedTrack.actions.filter((action: any) => action.editorRef.otherIds.indexOf(this.id) > -1);
    }
    get selectedTrack() {
        return <MyTrack> MyTimeline.instance.selectedTrack;
    }

    // for the Action just an extention

    addTo(clip: any) {
        return this.origAction.addTo(clip);
    }

    removeFrom() {
        return this.origAction.removeFrom();
    }

    update(time: number) {
        return this.origAction.update(time);
    }

    editorHTML(content: HTMLElement) {
        return this.origAction.editorHTML(content);
    }

    get id() {
        return this.origAction.id;
    }
    get clipId() {
        return this.origAction.clipId;
    }
    set clipId(value: number) {
        this.origAction.clipId = value;
    }
    get name() {
        return this.origAction.name;
    }
    set name(value: string) {
        this.origAction.name = value;
    }
    get startTime() {
        return this.origAction.startTime;
    }
    set startTime(value: number) {
        this.origAction.startTime = value.roundToTenth();
        this.render();
    }
    get endTime() {
        return this.origAction.endTime;
    }
    // get the Action id's it depends on
    get otherIds(): Array<number> {
        return this.origAction.otherIds || [];
    }
    get isValid() {
        return this.origAction.isValid;
    }
}