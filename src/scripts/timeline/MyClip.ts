import { Clip, Matrix } from "lecture-timeline";
import MyTimeline from "./MyTimeline";

import TimelinePane from "./TimelinePane";
import { clamp, rand, svgTransform } from "../utils";
import MyTrack from "./MyTrack";

interface IDragInfo {
    temp: boolean;
    drag: boolean;
    x: number;
    startTime: number;
}

export default class MyClip extends Clip {

    content: SVGGElement;
    renderHeight = 80;

    onDocumentInteractionBound: Function;
    dragInfo: IDragInfo = <IDragInfo> {};

    audioBuffer: AudioBuffer;

    constructor(data: any | null = null) {
        super(data);

        const resource = MyTimeline.instance.loader.getResource(this.resourceName);

        this.name = resource.title;
        this.audioBuffer = resource.audioBuffer;
        this.audioLength = resource.audioBuffer.duration;

        // our content
        this.content = document.createElementNS("http://www.w3.org/2000/svg", "g");
        this.content.classList.add("clip");
        this.content.setAttributeNS(null, "transform", "translate(0, 0)");
        
        // draw the peaks
        this.drawCanvas();
        // render
        this.render();

        // our events
        this.onDocumentInteractionBound = (event: MouseEvent) => this.onDocumentInteraction(event);
        document.addEventListener("mousedown", this.onDocumentInteractionBound as EventListener);
    }

    drawCanvas() {
        let svgWidth = Math.floor(this.audioLength * TimelinePane.instance.oneSecondLength);
        let svgHeight = this.renderHeight;

        // get ready to draw the peaks
        const channel = this.audioBuffer.getChannelData(0);

        const steps = Math.floor(channel.length / svgWidth);
        let x = 0;

        let sums = [];
        for (let i = 0; i < channel.length; i += steps) {
            // gets the sum
            let value = 0;
            for (let j = 0; j < steps; j += 100) {
                value += Math.abs(channel[i + j]);
            }
            // save the average
            if (!isNaN(value)) {
                sums.push(value / (steps / 100));
            }
        }

        // normalize so we get the highest value
        let multiplier = Math.pow(Math.max(...sums), -1);
        let filtered = sums.map(n => n * multiplier);

        let points: string = "";
        // now with a normalized array let's draw our wave
        filtered.forEach((n: number, index: number) => {
            points += x + "," + (!index || index === filtered.length - 1 ? 0 : -n * svgHeight).toFixed(2) + " ";
            x++;
        });

        this.content.insertAdjacentHTML("beforeend", `<rect fill="rgba(0, 0, 0, 0)" y="${-svgHeight}" width="${svgWidth}" height="${svgHeight}" />`);
        this.content.insertAdjacentHTML("beforeend", `<polyline fill="rgba(${rand(100, 200)}, ${rand(100, 200)}, ${rand(100, 200)}, 0.6)" points="${points}" />`);
        this.content.insertAdjacentHTML("beforeend", `<text x="5" y="${-this.renderHeight + 55}">${this.name}</text>`);
    }

    onDocumentInteraction(event: MouseEvent) {
        // can't interact when playing
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        switch (event.type) {
            // if the user clicked our canvas
            case "mousedown": {
                if (!event.button && event.target === this.content.querySelector("rect")) {
                    this.dragInfo.temp = true;
                    this.dragInfo.x = event.offsetX;
                    this.dragInfo.startTime = TimelinePane.instance.drawer.getTime(event.offsetX) - this.startTime;

                    // apply additional events
                    document.addEventListener("mousemove", this.onDocumentInteractionBound as EventListener);
                    document.addEventListener("mouseup", this.onDocumentInteractionBound as EventListener);
                }
                break;
            }
            case "mousemove": {
                if (this.dragInfo.temp) {
                    // did the user drag it far enough to be considered draggable?
                    if (Math.abs(event.offsetX - this.dragInfo.x) >= 8) {
                        this.dragInfo.temp = false;
                        this.dragInfo.drag = true;
                    }
                }
                // user is dragging
                if (this.dragInfo.drag) {
                    // keep the Actions tied to this Clip
                    const actions = this.selectedTrack.actions.filter(action => action.startTime >= this.startTime && action.startTime <= this.endTime && action.clipId === this.id);

                    // keep reference to old start time
                    const oldStartTime = this.startTime;

                    // set new startTime
                    this.startTime = (TimelinePane.instance.drawer.getTime(event.offsetX) - this.dragInfo.startTime).roundToTenth();

                    // adjust to the difference
                    const diff = this.startTime - oldStartTime;
                    actions?.forEach((action: any) => action.editorRef.startTime += diff);

                    this.render();

                    // TODO - fine tune this?
                    this.selectedTrack.adjustActionsDependencies();

                    // make the drawer updateTime
                    TimelinePane.instance.drawer.dispatchEvent(new CustomEvent("updateTime"));
                }

                break;
            }
            case "mouseup": {
                if (this.dragInfo.drag) {
                    // sort then check
                    MyTimeline.instance.selectedTrack?.sortClips().checkClips();
                    // dispatch the event
                    TimelinePane.instance.dispatchEvent(new CustomEvent("clipUpdate"));
                }

                this.dragInfo.temp = false;
                this.dragInfo.drag = false;

                // remove the additional events
                document.removeEventListener("mousemove", this.onDocumentInteractionBound as EventListener);
                document.removeEventListener("mouseup", this.onDocumentInteractionBound as EventListener);

                break;
            }
        }
    }

    destroy() {
        // visually remove it
        this.content.remove();

        // remove events
        document.removeEventListener("mousedown", this.onDocumentInteractionBound as EventListener);
    }

    // resize the rect and polyline with the Drawer's zoom value
    render() {
        // minus 40 because of the times
        const height = 40;

        const rect = this.content.querySelector("rect")!;
        const polyline = this.content.querySelector("polyline")!;
        rect.style.transform = polyline!.style.transform = `scaleX(${TimelinePane.instance.zoom}) scaleY(${(this.renderHeight - height) / this.renderHeight})`;

        svgTransform(this.content, this.startTime * TimelinePane.instance.oneSecondZoom, TimelinePane.instance.element.offsetHeight - 12 - TimelinePane.instance.toolbar.element.offsetHeight);

        return this;
    }

    selected(flag: boolean) {
        this.content.querySelector("rect")!.classList[flag ? "add" : "remove"]("selected");
        return this;
    }

    error(flag: boolean) {
        this.content.querySelector("rect")!.classList[flag ? "add" : "remove"]("error");
        return this;
    }
    

    get selectedTrack() {
        return <MyTrack> MyTimeline.instance.selectedTrack;
    }
}