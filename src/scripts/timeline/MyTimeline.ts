import { ActionType, Group, Timeline, Track } from "lecture-timeline";
import StateManager from "../StateManager";
import MyAction from "./MyAction";
import MyClip from "./MyClip";
import MyTrack from "./MyTrack";

export default class MyTimeline extends Timeline {
    static instance: MyTimeline;

    // selectedTrack
    _selectedTrack: MyTrack | null;

    constructor() {
        super({
            content: "#timeline-local #contentWindow",
            subtitles: "#timeline-local #contentSubtitles"
        });

        // make an instance of it
        MyTimeline.instance = this;

        // output the log?
        this.showLog = false;

        // disable interaction locally
        this.interaction.disable();

        this.loader.rootPath = "https://ttv5.s3.amazonaws.com/"; // dev
        // this.loader.rootPath = "https://cloud.teachingtextbooks.com/"; // production

        window.addEventListener("message", (event) => {
            const data = event.data;

            switch (data.type) {
                case "import-group": {
                    const group = new Group(data.group);
                    this.selectedTrack?.addGroup(group);

                    // go to the time locally
                    this.jumpTo(this.time);

                    // overwrite rect and matrix from the frame so it's more accurate
                    group.setRectAndMatrix(data.group.rect, data.group.matrix);

                    StateManager.instance.record();
                    break;
                }
                case "import-group-remove": {
                    const group = this.getGroup(data.groupId) as unknown as Group;
                    group.track.removeGroup(group);

                    this.jumpTo(this.time);

                    StateManager.instance.record();
                    break;
                }
                case "import-group-update": {
                    (this.getGroup(data.groupId) as unknown as Group).duration = data.duration;

                    this.jumpTo(this.time);

                    StateManager.instance.record();
                    break;
                }
                case "viewer-bounds": {
                    const parent = this.content.parentElement!;
                    parent.style.width = `${data.rect.width}px`;
                    parent.style.height = `${data.rect.height}px`;
                    break;
                }
            }
        });
    }

    setupEvents() {}

    override addTrack(myTrack: MyTrack) {
        super.addTrack(myTrack as Track);
        this.selectedTrack = myTrack;
        return myTrack;
    }

    override removeTrack(myTrack: MyTrack) {
        // removes MyClips and MyActions
        myTrack.destroy();

        super.removeTrack(myTrack as Track);
        
        // revert to the Main
        if (!myTrack.isMain) {
            this.selectedTrack = this.tracks[0] as MyTrack || null;
        }

        return this;
    }

    override empty() {
        while (this.tracks.length) {
            const myTrack = this.tracks.pop();
            if (myTrack) {
                this.removeTrack(myTrack as MyTrack);
            }
        }

        // nothing selected
        this.selectedTrack = null;
        
        super.empty();
    }

    override async import(data: any) {
        this.empty();

        // add the Tracks
        await Promise.all(data.tracks.map(async (track: any) => {
            // copy the Actions and Clips JSON object, we need to make classes
            const actions = track.actions.slice();
            const clips = track.clips.slice();
            track.actions.length = track.clips.length = 0;

            // create the Track
            const myTrack = this.addTrack(new MyTrack(track));

            // load the Action's resources
            await Promise.all(actions.map(async (action: any) => {
                await Promise.all((action.resourceNames || []).map(async (name: string) => {
                    await this.loader.loadResource(name);
                }));
            }));

            // now add the Actions to the Track
            actions.forEach((action: any) => {
                const type = action.type as keyof typeof ActionType;
                const myAction = new MyAction(new ActionType[type](action));
                myTrack.addAction(myAction);
            });

            // load the Clip's resources
            await Promise.all(clips.map(async (clip: any) => {
                await this.loader.loadResource(clip.resourceName);
            }));

            // now add the Clips to the Track
            clips.forEach((clip: any) => {
                const myClip = new MyClip(clip);
                myTrack.addClip(myClip);
            });
        }));
    }

    set selectedTrack(value: MyTrack | null) {
        // deselect other Tracks
        this.tracks.forEach(track => (track as MyTrack).toggle(false));

        // show the selected
        this._selectedTrack = this.currentTrack = value?.toggle(true) || null;

        // let everyone know
        this.dispatchEvent(new CustomEvent("selectedTrack", {
            bubbles: true,
            detail: {
                track: value
            }
        }));
    }
    get selectedTrack() {
        return this._selectedTrack;
    }
}