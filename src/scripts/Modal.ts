import Hotkeys from "@teachingtextbooks/hotkeys";
import ContextMenu from "./contextmenu";

interface IDragInfo {
    isMouseDown: boolean;
    drag: boolean;
    x: number;
    y: number;
};

export default class Modal {
    static instance: Modal;

    element: HTMLElement;
    elements: {[element: string]: HTMLElement} = {};

    dragInfo: IDragInfo = <IDragInfo> {};

    constructor() {
        Modal.instance = this;

        const id = "modal";

        this.element = document.createElement("div");
        this.element.classList.add(id);
        document.body.appendChild(this.element);

        this.element.innerHTML = `
            <div class="${id}__wrapper">
                <div class="${id}__title"></div>
                <div class="${id}__content"></div>
            </div>
        `;

        this.elements.wrapper = this.element.querySelector(`.${id}__wrapper`) as HTMLElement;
        this.elements.title = this.element.querySelector(`.${id}__title`) as HTMLElement;
        this.elements.content = this.element.querySelector(`.${id}__content`) as HTMLElement;

        // default
        this.hide();
    }

    setupEvents() {
        Hotkeys.on("escape", () => this.hide());

        document.addEventListener("mousedown", (event) => this.onInteraction(event));
        document.addEventListener("mousemove", (event) => this.onInteraction(event));
        document.addEventListener("mouseup", (event) => this.onInteraction(event));
        document.addEventListener("click", (event) => this.onInteraction(event));

        window.addEventListener("message", (event: MessageEvent) => {
            switch (event.data.type) {
                case "modal-hide": {
                    this.hide();
                    break;
                }
            }
        });
    }

    onInteraction(event: MouseEvent) {
        switch (event.type) {
            case "mousedown": {
                if (event.target === this.elements.title) {
                    this.dragInfo.drag = true;

                    const style = window.getComputedStyle(this.elements.wrapper);
                    this.dragInfo.x = event.clientX - parseInt(style.left);
                    this.dragInfo.y = event.clientY - parseInt(style.top);
                }
                break;
            }
            case "mousemove": {
                if (this.dragInfo.drag) {
                    const wrapperStyle = this.elements.wrapper.style;
                    wrapperStyle.left = `${event.clientX - this.dragInfo.x}px`;
                    wrapperStyle.top = `${event.clientY - this.dragInfo.y}px`;
                }
                break;
            }
            case "mouseup": {
                this.dragInfo.drag = false;
                break;
            }
            case "click": {
                if (event.target === this.element) {
                    this.hide();
                }
                break;
            }
        }
    }

    show(title: string = "", content: string = "") {
        if (this.isShowing) {
            return;
        }

        ContextMenu.instance.hide();

        this.elements.title.innerHTML = title;
        this.elements.content.innerHTML = content;

        const wrapperStyle = this.elements.wrapper.style;
        wrapperStyle.top = wrapperStyle.left = "50%";
        
        this.element.style.display = "block";
        setTimeout(() => this.element.classList.add("open"));
    }

    hide() {
        if (!this.isShowing) {
            return;
        }

        const self = this;
        this.element.classList.remove("open");
        this.element.addEventListener("transitionend", function transitionEnd() {
            self.element.removeEventListener("transitionend", transitionEnd);
            self.element.style.display = "none";

            // clear it out
            self.elements.title.innerHTML = "";
            self.elements.content.innerHTML = "";
        });
    }

    get isShowing() {
        return this.element.classList.contains("open");
    }
}