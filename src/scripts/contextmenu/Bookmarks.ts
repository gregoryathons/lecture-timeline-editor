
import ContextMenu, { ContextMenuEvent } from ".";
import MyTimeline from "../timeline/MyTimeline";
import TimelinePane from "../timeline/TimelinePane";
import { secondsToString } from "../utils";

export default class Bookmarks {
    constructor() {
        ContextMenu.instance.addEventListener(ContextMenuEvent.Prep, (event: Event) => this.onContextMenu(event as CustomEvent));
        ContextMenu.instance.addEventListener(ContextMenuEvent.Selected, (event: Event) => this.onContextMenu(event as CustomEvent));
    }

    onContextMenu(event: CustomEvent) {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        let { target, offsetX } = event.detail.origEvent;
        const element = target as HTMLElement;
        const time = TimelinePane.instance.drawer.getTime(offsetX);

        switch (event.type) {
            case ContextMenuEvent.Prep: {
                if (!element.closest("#timeline-drawer-wrapper")) {
                    return;
                }

                // set the time 
                TimelinePane.instance.time = time;

                // closest bookmark
                const bookmarks = this.selectedTrack.bookmarks;
                const bookmark = bookmarks.find(bm => Math.abs(time - bm.time) <= 0.1);

                const list = [];
                if (bookmark) {
                    list.push({ name: `Remove "${bookmark.name}" Bookmark`, value: bookmarks.indexOf(bookmark), action: "remove-bookmark" });
                }
                else {
                    list.push({ name: "Add Bookmark", action: "add-bookmark" });
                    if (bookmarks.length) list.push(null);
                    bookmarks.forEach(bm => list.push({ 
                        name: `${bm.name} <small>(${secondsToString(bm.time)})</small>`, 
                        value: bm.time, 
                        action: "goto-bookmark"
                    }));
                }

                // show the context menu
                ContextMenu.instance.addItem(list);

                break;
            }
            case ContextMenuEvent.Selected: {
                const { action, value } = element.dataset;

                if (action === "add-bookmark") {
                    const bookmarkName = prompt("Add Bookmark", "")?.trim();
                    if (bookmarkName) {
                        this.selectedTrack.addBookmark(bookmarkName, TimelinePane.instance.time);
                    }
                }
                else if (action === "remove-bookmark") {
                    const bookmark = this.selectedTrack.bookmarks[parseInt(value!)];
                    this.selectedTrack.removeBookmark(bookmark);
                }
                // user must have selected a bookmark, jump to that time
                else if (action === "goto-bookmark") {
                    TimelinePane.instance.drawer.onTimelineUpdate(parseFloat(value!));
                }

                break;
            }
        }
    }

    get selectedTrack() {
        return MyTimeline.instance.selectedTrack!;
    }
};