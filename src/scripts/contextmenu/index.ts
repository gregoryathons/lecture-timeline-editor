import { debounce } from "../utils";
import Bookmarks from "./Bookmarks";
import Actions from "./Actions";

const className = "context-menu";

export const ContextMenuEvent = {
    Prep: "prep",
    Show: "show",
    Hide: "hide",
    Selected: "selected"
};

interface IItem {
    name: string;
    icon?: string;
    value?: string | number;
    action: string;
};
interface ITab {
    name: string;
    items: Array<IItem>;
};

export default class ContextMenu extends EventTarget {
    static instance: ContextMenu;

    ftnShow: Function;
    tabs: Array<ITab> = [];
    items: Array<IItem | null> = [];

    element: HTMLElement;
    x = 0;
    y = 0;

    constructor(classes: any = []) {
        super();

        ContextMenu.instance = this;

        // init our classes
        classes.forEach((klass: any) => new klass());

        // will be executed
        this.ftnShow = debounce(() => {
            this._show(); // show the items
            this.tabs.length = this.items.length = 0; // remove so it's cleaned out
        });

        // creates our element
        this.element = document.createElement("div");
        this.element.classList.add(className);
        this.element.hidden = true;
        document.body.appendChild(this.element);

        // setup the events
        this.setupEvents();
    }

    setupEvents() {
        window.addEventListener("message", (event) => {
            switch (event.data.type) {
                case "contextmenu": {

                    this.dispatch(ContextMenuEvent.Prep, { 
                        target: document.body,
                        pageX: event.data.pageX, 
                        pageY: event.data.pageY 
                    } as unknown as MouseEvent);

                    break;
                }
            }
        });

        window.addEventListener("contextmenu", (event) => {
            event.preventDefault();
            this.dispatch(ContextMenuEvent.Prep, event as MouseEvent);
        });

        window.addEventListener("mousedown", (event) => {
            const element = event.target as HTMLElement;

            if (element.closest(`.${className}`)) {
                
                // not on the HR tag
                if (element.tagName.toLowerCase() === "hr") {
                    event.preventDefault();
                    return;
                }

                // clicked on a tab button
                if (element.tagName.toLowerCase() === "button") {
                    this.showTab(parseInt(element.dataset.index!));
                    return;
                }

                this.dispatch(ContextMenuEvent.Selected, event);
            }

            // hide it
            this.hide();
        });

        window.addEventListener("resize", () => this.hide());
        window.addEventListener("blur", () => this.hide());
    }

    showTab(index: number) {
        const buttons = Array.from(this.element.querySelectorAll(`button[data-index]`)) as Array<HTMLElement>;
        const divs = Array.from(this.element.querySelectorAll(`div[data-index]`)) as Array<HTMLElement>;

        if (!divs.length) {
            return;
        }

        buttons.forEach(b => b.classList.remove("active"));
        divs.forEach(d => d.classList.remove("active"));

        buttons.find(b => parseInt(b.dataset.index!) === index)!.classList.add("active");
        divs.find(d => parseInt(d.dataset.index!) === index)!.classList.add("active");
    }

    addItem(items: Array<IItem | null>) {
        if (this.items.length) {
            this.items.push(null);
        }
        this.items = this.items.concat(items);

        // show it off
        this.ftnShow();
    }

    addTab(tab: ITab) {
        this.tabs.push(tab);

        // show it off
        this.ftnShow();
    }

    private _show() {
        this.element.hidden = false;
        this.element.style.inset = `${this.y}px auto auto ${this.x}px`;

        const addItem = (item: IItem) => {
            return `
                <div class="${className}__item" data-action="${item.action}" ${typeof item.value !== "undefined" ? `data-value="${item.value}"` : ""}>
                    ${item.icon ? `<span class="material-icons">${item.icon}</span>` : ""}${item.name}
                </div>
            `;
        };

        let html = "";
        let tabIndex = 0; // reference to the highest items in the tab

        if (this.items.length) {
            this.tabs.length = 0;

            this.items.forEach(item => {
                html += !item ? `<hr />` : addItem(item);
            });
        }
        else if (this.tabs.length) {
            this.items.length = 0;

            // tab names
            html += `<div class="${className}__tabs">`;
            this.tabs.forEach((tab, index) => html += `<button data-index="${index}">${tab.name}</button>`);
            html += `</div>`;

            // tab items
            this.tabs.forEach((tab, index) => {
                html += `<div class="${className}__content" data-index="${index}">`;
                tab.items.forEach(item => html += addItem(item));
                html += `</div>`;

                if (tab.items.length > tabIndex) {
                    tabIndex = index;
                }
            });
        }
        
        // render it
        this.element.innerHTML = html;

        // adjust the width :(
        this.element.style.width = this.tabs.length ? `${this.tabs.length * 120}px` : "auto";

        // show the tab that has the most items, so we get bounds
        this.showTab(tabIndex);

        // make sure it's in the window
        const bounds = this.element.getBoundingClientRect();
        if (bounds.bottom > window.innerHeight) {
            this.y -= bounds.bottom - window.innerHeight + 8;
        }
        if (bounds.right > window.innerWidth) {
            this.x -= bounds.right - window.innerWidth + 8;
        }

        // reposition again
        this.element.style.inset = `${this.y}px auto auto ${this.x}px`;
        this.element.classList.add("show");

        // let every know
        this.dispatch(ContextMenuEvent.Show);

        // show the tab
        this.showTab(0);
    }

    hide() {
        this.element.classList.remove("show");//hidden = true;
        // this.element.innerHTML = ``;

        this.dispatch(ContextMenuEvent.Hide);
    }

    dispatch(type: string, event: MouseEvent | null = null) {
        if (event) {
            this.x = event.pageX;
            this.y = event.pageY;
        }

        this.dispatchEvent(new CustomEvent(type, { 
            detail: {
                origEvent: event
            } 
        }));
    }

    get isShowing() {
        return !this.element.hidden;
    }
};

// init
new ContextMenu([Actions, Bookmarks]);