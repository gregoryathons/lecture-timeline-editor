import { ActionType } from "lecture-timeline";
import ContextMenu, { ContextMenuEvent } from ".";
import MyAction, { findOpening } from "../timeline/MyAction";
import MyTimeline from "../timeline/MyTimeline";
import TimelinePane from "../timeline/TimelinePane";

export default class Actions {
    constructor() {
        ContextMenu.instance.addEventListener(ContextMenuEvent.Prep, (event: Event) => this.onContextMenu(event as CustomEvent));
        ContextMenu.instance.addEventListener(ContextMenuEvent.Selected, (event: Event) => this.onContextMenu(event as CustomEvent));
    }

    onContextMenu(event: CustomEvent) {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        let { target } = event.detail.origEvent;
        const element = target as HTMLElement;

        switch (event.type) {
            case ContextMenuEvent.Prep: {
                if (!element.closest("#timeline-drawer-wrapper") && element !== document.body) {
                    return;
                }

                if (element === document.body) {
                    this.listActions();
                }
                else {
                    ContextMenu.instance.addItem([{ name: "Add Action", action: "add-action" }]);
                }

                break;
            }
            case ContextMenuEvent.Selected: {
                const { action, value } = element.dataset;

                if (action === "add-action") {
                    this.listActions();
                }
                // user selected the action
                else if (action === "add-action-confirm") {
                    this.selectedAction(value!);
                }

                break;
            }
        }
    }

    listActions() {
        const itemsTimeline = [
            { name: "Play", icon: "play_arrow", value: "Play", action: "add-action-confirm" },
            { name: "Pause", icon: "pause", value: "Pause", action: "add-action-confirm" },
            { name: "SFX", icon: "music_note", value: "SFX", action: "add-action-confirm" }
        ];
        const itemsElements = [
            { name: "Text", icon: "e_mobiledata", value: "AddElement|Text", action: "add-action-confirm" },
            { name: "Header", icon: "title", value: "AddElement|Header", action: "add-action-confirm" },
            { name: "Image", icon: "image", value: "AddElement|Image", action: "add-action-confirm" },
            { name: "SVG", icon: "pentagon", value: "AddElement|SVG", action: "add-action-confirm" },
            { name: "Tip", icon: "note", value: "AddElement|Tip", action: "add-action-confirm" },
            { name: "Table Chart", icon: "table_chart", value: "AddElement|Table", action: "add-action-confirm" },
            { name: "Update Text", icon: "edit", value: "UpdateText", action: "add-action-confirm" },
            { name: "Tween", icon: "animation", value: "TweenElement", action: "add-action-confirm" },
            { name: "Remove", icon: "remove", value: "RemoveElement", action: "add-action-confirm" }
        ];
        const itemsStyles = [
            { name: "Add", icon: "add", value: "AddStyle", action: "add-action-confirm" },
            { name: "Remove", icon: "remove", value: "RemoveStyle", action: "add-action-confirm" }
        ];
        const itemsInteractions = [
            { name: "Click", icon: "ads_click", value: "Click", action: "add-action-confirm" },
            { name: "Input", icon: "input", value: "Input", action: "add-action-confirm" },
            { name: "Drag and Drop", icon: "open_with", value: "DragDrop", action: "add-action-confirm" }
        ];
        const itemsEffects = [
            { name: "Typing Text", icon: "text_rotation_none", value: "TypingText", action: "add-action-confirm" },
            { name: "Fade In", icon: "star", value: "FadeIn", action: "add-action-confirm" },
            { name: "Fade Out", icon: "star", value: "FadeOut", action: "add-action-confirm" },
            { name: "Pop In", icon: "star", value: "PopIn", action: "add-action-confirm" },
            { name: "Pop Out", icon: "star", value: "PopOut", action: "add-action-confirm" },
            { name: "Float In", icon: "arrow_downward", value: "FloatIn", action: "add-action-confirm" },
            { name: "Float Out", icon: "arrow_upward", value: "FloatOut", action: "add-action-confirm" },
            { name: "Zoom In", icon: "zoom_in_map", value: "ZoomIn", action: "add-action-confirm" },
            { name: "Zoom Out", icon: "zoom_out_map", value: "ZoomOut", action: "add-action-confirm" },
            { name: "Pulse", icon: "star", value: "Pulse", action: "add-action-confirm" },
            { name: "Wiggle", icon: "star", value: "Wiggle", action: "add-action-confirm" }
        ];

        ContextMenu.instance.addTab({ name: "Timeline", items: itemsTimeline });
        ContextMenu.instance.addTab({ name: "Elements", items: itemsElements });
        ContextMenu.instance.addTab({ name: "Styles", items: itemsStyles });
        ContextMenu.instance.addTab({ name: "Interactions", items: itemsInteractions });
        ContextMenu.instance.addTab({ name: "Effects", items: itemsEffects });
    }

    selectedAction(value: string) {
        const v = value.split("|");
        const type = v[0] as keyof typeof ActionType;

        // default options
        const options = { 
            clipId: this.selectedClip?.id || 0,
            startTime: this.time 
        } as any;

        // it was an "AddElement" type
        if (v[0] === "AddElement") {
            options.rect = new DOMRect();
            options.elementType = v[1];

            if (v[1] === "Image") {
                options.resourceNames = ["PLACEHOLDER"];
            }

            findOpening(MyTimeline.instance, options.rect);
        }

        // create the new Action
        const myAction = new MyAction(new ActionType[type](options));
        this.selectedTrack?.addAction(myAction);
    }

    get selectedTrack() {
        return MyTimeline.instance.selectedTrack;
    }
    get selectedClip() {
        return this.selectedTrack?.selectedClip;
    }
    get time() {
        return TimelinePane.instance.time;
    }
};