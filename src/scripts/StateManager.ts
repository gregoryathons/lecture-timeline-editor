import App from "./App";
import StagePane from "./stage/StagePane";
import MyTimeline from "./timeline/MyTimeline";
import TimelinePane from "./timeline/TimelinePane";
import { debounce, trace } from "./utils";

interface IState {
    time: number;
    timeline: string;
    trackId: number; 
};

export const StateManagerEvent = {
    Updated: "updated",
    Undo: "undo",
    Redo: "redo"
};

export default class StateManager extends EventTarget {
    static instance: StateManager;

    enabled = true;
    states: Array<IState> = [];
    index = -1;

    record: Function;

    constructor() {
        super();

        StateManager.instance = this;

        this.record = debounce(this.recordState);
    }

    setupEvents() {}

    recordState() {
        if (TimelinePane.instance.isPlaying) {
            return;
        }

        if (this.enabled) {
            // our new state we will be recording
            const state = {
                time: TimelinePane.instance.time,
                timeline: JSON.stringify(MyTimeline.instance.export()),
                trackId: MyTimeline.instance.selectedTrack?.id || 0
            };

            // reset the length of the states
            if (this.index < this.states.length - 1) {
                this.states.length = this.index + 1;
            }
            
            this.index = this.states.push(state) - 1;

            this.dispatch(StateManagerEvent.Updated);
        }

        this.enabled = true;
    }

    async undo() {
        if (!this.enabled) {
            return;
        }

        if (this.index > 0) {
            this.enabled = false;

            const state = this.states[this.index - 1];
            this.index--;

            await this.updateToTimelineState(state);

            this.dispatch(StateManagerEvent.Undo);
        }
    }

    async redo() {
        if (!this.enabled) {
            return;
        }

        if (this.index < this.states.length - 1) {
            this.enabled = false;

            const state = this.states[this.index + 1];
            this.index++;

            await this.updateToTimelineState(state);

            this.dispatch(StateManagerEvent.Redo);
        }
    }

    async jumpTo(index: number) {
        if (!this.enabled) {
            return;
        }

        this.enabled = false;

        const state = this.states[index];
        this.index = index;

        await this.updateToTimelineState(state);

        this.dispatch(StateManagerEvent.Updated);
    }

    private dispatch(event: string) {
        this.dispatchEvent(new CustomEvent(event, { 
            detail: {
                index: this.index,
                length: this.states.length
            } 
        }));
    }

    private async updateToTimelineState(state: IState) {
        // remove old
        MyTimeline.instance.empty();

        // tell's the iFrame to remove everything
        // we are passing true, and typically this would save the state, but we have `this.enabled` = false
        // await StagePane.instance.updateDisplay(true, false);

        if (state) {
            await App.instance.loadTimeline(JSON.stringify(state));
        }
    }
}