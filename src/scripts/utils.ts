declare global {
    interface Number {
        round: (precision: number) => number;
        roundToTenth: () => number;
        floorToTenth: () => number;
    }
}

Number.prototype.round = function(precision: number): number {
    precision = Math.pow(10, precision || 0);
    return (Math.round(Number(this) * precision)) / precision;
};
Number.prototype.roundToTenth = function(): number {
    return Math.round(Number(this) * 10) / 10;
};
Number.prototype.floorToTenth = function(): number {
    return Math.floor(Number(this) * 10) / 10;
};

export function secondsToString(totalSeconds: number) {
    // const years = Math.floor(totalSeconds / 31536000);
    // const days = Math.floor((totalSeconds % 31536000) / 86400); 
    // const hours = Math.floor(((totalSeconds % 31536000) % 86400) / 3600);
    const minutes = Math.floor((((totalSeconds % 31536000) % 86400) % 3600) / 60);
    const seconds = (((totalSeconds % 31536000) % 86400) % 3600) % 60;

    return `${minutes}:${(seconds < 10 ? 0 : "") + seconds.toFixed(1)}`;
}

// SVGGraphicsElement
export function svgTransform(element: any, x: number | null = null, y: number | null = null, scaleX: number | null = null, scaleY: number | null = null): DOMMatrix | undefined {
    const transform = element.transform.baseVal.consolidate();

    if (x === null) {
        return transform.matrix;
    }
    else {
        transform.matrix.e = x;
        transform.matrix.f = y || transform.matrix.f;
        transform.matrix.a = scaleX || transform.matrix.a;
        transform.matrix.d = scaleY || transform.matrix.d;
    }
}

export const clamp = (num: number, min: number, max: number) => Math.min(Math.max(num, min), max);

export const rand = (min: number, max: number) => Math.floor(Math.random() * (max - min + 1) + min);

// swaps nodeA and nodeB if they have the same parent
export function swapNodes(nodeA: Node, nodeB: Node) {
    if (nodeA.parentNode === nodeB.parentNode) {
        const parentA = nodeA.parentNode!;
        const siblingA = nodeA.nextSibling === nodeB ? nodeA : nodeA.nextSibling;

        // move `nodeA` to before the `nodeB`
        nodeB.parentNode!.insertBefore(nodeA, nodeB);

        // move `nodeB` to before the sibling of `nodeA`
        parentA.insertBefore(nodeB, siblingA);
    }
};

// handy dany lerp
export const lerp = (start: number, end: number, t: number) => (1 - t) * start + t * end;

export function trace(...data: any) {
    console.groupCollapsed.apply(console.groupCollapsed, data);
    console.trace();
    console.groupEnd();
};

export function debounce(ftn: Function, time: number = 200) {
    let timeoutId: ReturnType<typeof setTimeout>;
    return function(this: any, ...args: any[]) {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(() => ftn.apply(this, args), time);
    };
};

export function debounceRAF(ftn: Function) {
    let raf: number;
    return function(this: any, ...args: any[]) {
        window.cancelAnimationFrame(raf);
        raf = window.requestAnimationFrame(() => ftn.apply(this, args));
    };
};