const path = require("path");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const outputDir = "dist";
const staticDir = "static";

module.exports = (env, argv) => {
    return {
        entry: {
            index: path.resolve(__dirname, "./src/index.ts")
        },
        output: {
            filename: "bundle.js",
            path: path.resolve(__dirname, outputDir)
        },
        devtool: argv.mode === "development" ? "inline-source-map" : false,
        module: {
            rules: [{
                test: /\.tsx?$/,
                loader: "ts-loader",
                exclude: /node_modules/
            }, {
                test: /\.(sa|sc)ss$/,
                use: ["style-loader", "css-loader", "sass-loader"]
            }, {
                test: /\.(png|svg|jpe?g|gif)$/i,
                type: "asset",
                generator: {
                    filename: `${staticDir}/images/[name][ext]`,
                }
            }, {
                test: /\.ttf$/i,
                type: "asset",
                generator: {
                    filename: `${staticDir}/fonts/[name][ext]`,
                }
            }]
        },
        resolve: {
            extensions: [".tsx", ".ts", ".js"],
        },
        optimization: {
            minimize: false
        },
        plugins: [
            new HtmlWebpackPlugin({
                title: "Lesson Editor",
                template: "./src/index.html"
            }),
            new CopyWebpackPlugin({
                patterns: [{
                    from: `./src/${staticDir}`,
                    to: staticDir,
                    noErrorOnMissing: true
                }]
            })
        ]
    };
}